# ti-ai-contributor-application

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).
ATBB3nANsWktckJzpUtyNyN6K2FAB84E7C99
## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

# Install project
npm install

### Compile and Hot-Reload for Development

# For local development
npm run dev

# Build and deploy
npm run deploy

# Build
npm run build

# Install Firebase tools
npm install -g firebase-tools -g

# Firebase login
firebase login

# Initiate hosting
firebase init hosting

# Fonts Numbers
Reddit Mono
Reddit Sans Condensed
Reddit Sans
Rubik
Plus Jakarta Sans
Barlow
Barlow Condensed
Barlow Semi Condensed
Quicksand
Bebas Neue
Dosis
Abel
Outfit
Space Grotesk
Teko
Comfortaa
Radjhani
Asap
Teachers
Urbanist
Lexend Deca
Albert Sans
Khand
Advent Pro
Ropa Sans
Bai Jamjuree
JuraMOnda
Unica One
Monda
Sofia Sans Condensed

# Git revert and reset
git revert HEAD

git reset HEAD~1

# Sites
https://realfavicongenerator.net/

https://www.onlinewebfonts.com/
https://www.python.org/downloads/macos/

# In case build fail try this lines:
npm cache clean --force
rm -rf node_modules package-lock.json
npm install
npm run build
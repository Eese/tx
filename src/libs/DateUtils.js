import { handleEmptyString, handleIfNotNull, handleInstanceOf, handleNumber, handleTime } from './errorHandling'

const DAYS_OF_WEEK = 7;
const TOTAL_DAYS   = 42; 

const monthNameList     = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const dayNameList     = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const monthFullNameList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const base1MonthNames   = monthFullNameList.slice(); 
      base1MonthNames.unshift('');

function getMonday(time){
	handleTime(handleNumber(time, 'time'), 'time');
	let inputDate  = new Date(time);
	let dayOfWeek  = inputDate.getDay();
	let difference = dayOfWeek === 0 ? -6 : 1 - dayOfWeek;
	let monday     = new Date(inputDate.setDate(inputDate.getDate() + difference)).getTime();
	return monday
}

function getSunday(time){
	handleTime(handleNumber(time, 'time'), 'time');
	const inputDate  = new Date(time);
	const dayOfWeek  = inputDate.getDay();
	const difference = dayOfWeek === 0 ? -6 : 1 - dayOfWeek;
	const monday     = new Date(inputDate.setDate(inputDate.getDate() + difference));
	let sunday       = new Date(monday.getTime());
		sunday.setDate(monday.getDate() + 6);
		sunday       = sunday.getTime();
	return sunday
}

function getDate1(time){
	handleIfNotNull(handleTime, time, 'time');
	const date = null == time ? new Date() : new Date(time);
	      date.setHours(0,0,0,0);
	      date.setDate(1);
	return date
}

function getTimeDate1(time){
	handleIfNotNull(handleTime, time, 'time');
	const date = getDate1(time);
	return date.getTime()
}

function getLastMonthTime(time){
	handleTime(time, 'time');

	let lastDayofMonth    = new Date(time);
		lastDayofMonth.setMonth(lastDayofMonth.getMonth() + 1);
		lastDayofMonth.setDate(0);
	return lastDayofMonth.getTime()
}


function getDateZeroHour(time){
	handleIfNotNull(handleTime, time, 'time');
	const date = null == time ? new Date() : new Date(time);
	      date.setHours(0,0,0,0);
	return date
}

function getTimeZeroHour(time){
	handleIfNotNull(handleTime, time, 'time');
	const date = null == time ? new Date() : new Date(time);
	      date.setHours(0,0,0,0);
	return date.getTime()
}

const getMonthData = function (initialTime, isMondayBased) {
	handleNumber(initialTime, `initialTime`);

	const sundayIndexList = [0, 1, 2, 3, 4, 5, 6];
	const mondayIndexList = [6, 0, 1, 2, 3, 4, 5];

	let dayIndexList    = isMondayBased ? mondayIndexList : sundayIndexList;


	// Reset to first year, month, day and 00:00 hours
	const f_date = new Date(initialTime);
	      f_date.setHours(0, 0, 0, 0);
	      f_date.setDate(1);

	const f_time  = f_date.getTime();
	const f_month = f_date.getMonth();
	const f_year  = f_date.getFullYear();

	// First time of the month
	const f_firstMonthTime = f_date.getTime();

	// Get sunday of first week
	let indexDay     = dayIndexList[ f_date.getDay() ];
	const daysBefore = indexDay;

	f_date.setDate(-indexDay+1);
	const f_firstDayWeekTime = f_date.getTime();

	// Last time of the month
	f_date.setTime(f_time);
	f_date.setMonth(f_month+1);
	f_date.setDate(0);
	const f_lastMonthTime = f_date.getTime();

	// Last Saturday of the week
	indexDay        = dayIndexList[ f_date.getDay() ] + 1;
	const daysAfter = DAYS_OF_WEEK - indexDay;
	f_date.setDate(f_date.getDate()+daysAfter);
	const f_lastDayWeekTime = f_date.getTime();
	
	const f_beforeList   = [];
	const f_daysList     = [];
	const f_afterList    = [];
	const f_gridDaysList = [];
	const stringList     = [];

	f_date.setTime(f_firstDayWeekTime);

	let currentMonth;
	let loopTime;
	for(let index=0; index < TOTAL_DAYS; index++){
		loopTime = f_date.getTime();
		currentMonth = f_date.getMonth();

		if(currentMonth < f_month){
			f_beforeList.push(loopTime);
		}

		if(currentMonth == f_month){
			f_daysList.push(loopTime);
		}
		

		if(currentMonth > f_month){
			f_afterList.push(loopTime);
		}

		f_gridDaysList.push(loopTime);

		stringList.push(f_date.toDateString());

		f_date.setDate(f_date.getDate()+1);

    }

	const f_daysBefore  = f_beforeList.length;
	const f_daysAfter   = f_afterList.length;
	const f_monthLength = f_daysList.length;
	const f_daysLength  = daysBefore + f_monthLength + daysAfter;
	
	f_date.setTime(f_time);

	const finalMonthData = {
		date:	f_date,
		month:	f_month,
		year:	f_year,
		time:	f_time,

		firstMonthTime:   f_firstMonthTime,
		lastMonthTime:    f_lastMonthTime,

		firstDayWeekTime: f_firstDayWeekTime,
		lastDayWeekTime:  f_lastDayWeekTime,

		daysBefore:       f_daysBefore,
		daysAfter:        f_daysAfter,
		monthLength:      f_monthLength,
		daysLength:       f_daysLength,
		gridLengh:        TOTAL_DAYS,
     
		beforeList:       f_beforeList,
		daysList:         f_daysList,
		afterList:        f_afterList,
		gridDaysList:     f_gridDaysList,
		stringList,
		dayIndexList
	}

	return finalMonthData
}

const getDateWithZeroHours = function(){
	const date = new Date();
	      date.setHours(0,0,0,0);
	return date;
}

const resetToZeroHours = function(date){
	handleInstanceOf(date, Date, `date`);
	date.setHours(0,0,0,0);
}

const formatY_M_D = function(time){
	handleTime(time, 'time');
	const date = new Date(time);
	var year = date.toLocaleString("default", { year: "numeric" });
	var month = date.toLocaleString("default", { month: "2-digit" });
	var day = date.toLocaleString("default", { day: "2-digit" });
	return year+'-'+month+'-'+day
}

function getDateString(time){
	if(-1 == time){
		// Early return
		return ''
	}

	handleTime(time, 'time');
	const date = new Date(time);
	const dateString = date.getFullYear() + '/' + (date.getMonth()+1) + '/' + date.getDate();
	return dateString
}

function getDatesetFromString(inputDate){
	handleEmptyString(inputDate, 'inputDate');
	const dateArray   = inputDate.split('-');
	const year        = dateArray[0];
	const month       = parseInt(dateArray[1]); 
	const date        = parseInt(dateArray[2]);
	const dateObject  = new Date(`${base1MonthNames[month]} ${date}, ${year} 00:00:00`);
	const time        = dateObject.getTime();
	const dateString  = inputDate;
	const dateSlashed = dateArray.join('/');
	const dayIndex    = dateObject.getDay();
	const dayName     = dayNameList[dayIndex];
	const monthName   = monthNameList[dateObject.getMonth()];
	const isWeekEnd   = 0 == dayIndex || 6 == dayIndex;
	const isWeekDay   = 0 != dayIndex && 6 != dayIndex;
	const dateData = {
		dateString,
		dateSlashed,
		time,
		dayIndex,
		dayName,
		monthName,
		year,
		month,
		date,
		dateObject,
		isWeekEnd,
		isWeekDay
	}
	return dateData
}

function getDatesetFromTime(time){
	const dateString = formatY_M_D(time);
	return getDatesetFromString(dateString);
}

function formatFromDateString (date){
	handleEmptyString(date, 'date');
	const dateArray = date.split('-');
	const year      = dateArray[0];
	const month     = parseInt(dateArray[1]); 
	const day       = parseInt(dateArray[2]); 
	const time      = new Date(`${base1MonthNames[month]} ${day}, ${year} 00:00:00`).getTime();
	return time
}

function formatToDateString (time){
	handleTime(time, 'time');
	const date 		 = new Date(time);
	const year       = date.getFullYear();
	const month      = date.getMonth()+1; 
	const day        = date.getDate();
	const dateString = `${year}-${month < 10 ? '0'+ month: month}-${day < 10 ? '0'+day: day}`;
	return dateString
}

function getFormatedMinutes(minutesInput){
	handleNumber(minutesInput, 'minutesInput');
	let hours       = Math.floor(Math.abs(minutesInput) / 60);
	let hoursString = 0 == hours ? '' : hours + `${localize('hs')} `;
	let intMinutes  = Math.floor(Math.abs(minutesInput) % 60);
	let minutes     = intMinutes != 0 ? intMinutes + `${localize("'")}` : '';
	if(0 == hours && 0 == intMinutes){
		minutes = `0${localize("'")}`
	}
	return hoursString + minutes
}

export { 
	getMonthData,
	monthNameList,
	monthFullNameList,
	getDateWithZeroHours,
	resetToZeroHours,
	formatY_M_D,
	getMonday,
	getSunday,
	getDateZeroHour,
	getTimeZeroHour,
	getDate1,
	getTimeDate1,
	getLastMonthTime,
	getDateString,
	formatFromDateString,
	formatToDateString,
	getFormatedMinutes,
	dayNameList,
	getDatesetFromString,
	getDatesetFromTime
}
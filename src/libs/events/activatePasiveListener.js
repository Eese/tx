/*
Problem: All modern browsers have a threaded scrolling feature to permit scrolling
to run smoothly even when expensive JavaScript is running, but this optimization
is partially defeated by the need to wait for the results of any touchstart and 
touchmove handlers, which may prevent the scroll entirely 
by calling preventDefault() on the event.

Solution: {passive: true}
By marking a touch or wheel listener as passive, 
the developer is promising the handler won't call preventDefault to disable scrolling. 
This frees the browser up to respond to scrolling immediately without waiting for JavaScript, 
thus ensuring a reliably smooth scrolling experience for the user.
*/

const activatePasiveListener = function () {
  var supportsPassive = false;
  document.createElement("div").addEventListener("test", function () { }, {
    get passive() {
      supportsPassive = true;
      return false;
    }
  });

  if (!supportsPassive) {
    var super_add_event_listener = EventTarget.prototype.addEventListener;
    var super_remove_event_listener = EventTarget.prototype.removeEventListener;
    var super_prevent_default = Event.prototype.preventDefault;

    function parseOptions(type, listener, options, action) {
      var needsWrapping = false;
      var useCapture = false;
      var passive = false;
      var fieldId;
      if (options) {
        if (typeof (options) === 'object') {
          passive = options.passive ? true : false;
          useCapture = options.useCapture ? true : false;
        } else {
          useCapture = options;
        }
      }
      if (passive)
        needsWrapping = true;
      if (needsWrapping) {
        fieldId = useCapture.toString();
        fieldId += passive.toString();
      }
      action(needsWrapping, fieldId, useCapture, passive);
    }

    Event.prototype.preventDefault = function () {
      if (this.__passive) {
        return;
      }
      super_prevent_default.apply(this);
    }

    EventTarget.prototype.addEventListener = function (type, listener, options) {
      var super_this = this;
      parseOptions(type, listener, options,
        function (needsWrapping, fieldId, useCapture, passive) {
          if (needsWrapping) {
            var fieldId = useCapture.toString();
            fieldId += passive.toString();

            if (!this.__event_listeners_options)
              this.__event_listeners_options = {};
            if (!this.__event_listeners_options[type])
              this.__event_listeners_options[type] = {};
            if (!this.__event_listeners_options[type][listener])
              this.__event_listeners_options[type][listener] = [];
            if (this.__event_listeners_options[type][listener][fieldId])
              return;
            var wrapped = {
              handleEvent: function (e) {
                e.__passive = passive;
                if (typeof (listener) === 'function') {
                  listener(e);
                } else {
                  listener.handleEvent(e);
                }
                e.__passive = false;
              }
            };
            this.__event_listeners_options[type][listener][fieldId] = wrapped;
            super_add_event_listener.call(super_this, type, wrapped, useCapture);
          } else {
            super_add_event_listener.call(super_this, type, listener, useCapture);
          }
        });
    }

    EventTarget.prototype.removeEventListener = function (type, listener, options) {
      var super_this = this;
      parseOptions(type, listener, options,
        function (needsWrapping, fieldId, useCapture, passive) {
          if (needsWrapping &&
            this.__event_listeners_options &&
            this.__event_listeners_options[type] &&
            this.__event_listeners_options[type][listener] &&
            this.__event_listeners_options[type][listener][fieldId]) {
            super_remove_event_listener.call(super_this, type, this.__event_listeners_options[type][listener][fieldId], false);
            delete this.__event_listeners_options[type][listener][fieldId];
            if (this.__event_listeners_options[type][listener].length == 0)
              delete this.__event_listeners_options[type][listener];
          } else {
            super_remove_event_listener.call(super_this, type, listener, useCapture);
          }
        });
    }
  }
}

export { activatePasiveListener }
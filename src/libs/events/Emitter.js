import { handleNotValidString, handleObject, handleFunction, handleArray, handleEmptyString, handleIfNotNull } from '../errorHandling.js';

const Emitter = function (target, validTypes) {
	handleObject(target, 'target');
	handleIfNotNull(handleArray, validTypes, 'validTypes');

	const m_self = this;
	const m_target = target;
	const m_validTypes = null == validTypes ? [] : validTypes;
	const m_eventListeners = {};

	const length = m_validTypes.length;
	for (let index = 0; index < length; index++) {
		handleEmptyString(validTypes[index], 'validTypes[' + index + ']');
	}

	this.injectMethods = function () {
		m_target.addEventListener = function (type, methodToAdd, contextToAdd) {
			m_self.addEventListener.apply(m_self, [type, methodToAdd, contextToAdd]);
		}

		m_target.removeEventListener = function (type, methodToAdd, contextToAdd) {
			m_self.removeEventListener.apply(m_self, [type, methodToAdd, contextToAdd]);
		}

		m_target.addAllEventListeners = function (methodToAdd, contextToAdd) {
			m_self.addAllEventListeners.apply(m_self, [methodToAdd, contextToAdd]);
		}

		m_target.removeAllEventListeners = function (methodToAdd, contextToAdd) {
			m_self.removeAllEventListeners.apply(m_self, [methodToAdd, contextToAdd]);
		}
	}

	this.addEventListener = function (type, methodToAdd, contextToAdd) {
		if (0 < m_validTypes.length) {
			handleNotValidString(type, m_validTypes, 'type');
		}
		handleFunction(methodToAdd, 'methodToAdd');
		handleIfNotNull(handleObject, contextToAdd, 'contextToAdd');

		let list = m_eventListeners[type];
		if (null == list) {
			list = m_eventListeners[type] = [];
		}

		const iterator = list.entries();
		for (const [index, listener] of iterator) {
			if (methodToAdd == listener[0] && contextToAdd == listener[1]) {
				return;
			}
		}

		list.push([methodToAdd, contextToAdd]);
	}

	this.removeEventListener = function (type, methodToRemove, contextToRemove) {
		if (0 < m_validTypes.length) {
			handleNotValidString(type, m_validTypes, 'type');
		}
		handleFunction(methodToRemove, 'methodToRemove');
		handleIfNotNull(handleObject,  'handleObject');

		const list = m_eventListeners[type];
		if (null == list) {
			return;
		}

		const iterator = list.entries();
		for (const [index, listener] of iterator) {
			if (methodToRemove == listener[0] && contextToRemove == listener[1]) {
				list.splice(index, 1);
				return;
			}
		}
	}

	this.addAllEventListeners = function (methodToAdd, contextToAdd) {
		handleFunction(methodToAdd, 'methodToAdd');
		handleIfNotNull(handleObject, contextToAdd, 'contextToAdd');

		const length = m_validTypes.length;
		for (var index = 0; index < length; index++) {
			this.addEventListener(m_validTypes[index], methodToAdd, contextToAdd);
		}
	}

	this.removeAllEventListeners = function (methodToRemove, contextToRemove) {
		handleFunction(methodToRemove, 'methodToAdd');
		handleIfNotNull(handleObject, contextToRemove, 'contextToRemove');

		const length = m_validTypes.length;
		for (var index = 0; index < length; index++) {
			this.removeEventListener(m_validTypes[index], methodToRemove, contextToRemove);
		}
	}

	this.emitEvent = function (type, data) {
		if (0 < m_validTypes.length) {
			handleNotValidString(type, m_validTypes, 'type');
		}

		const list = m_eventListeners[type]
		if (null == list) {
			return
		}
		setTimeout(function () {
			list.forEach(listener => {
				const event = {}
				for (let prop in data) {
					event[prop] = data[prop];
				}
				event.type = type;
				event.target = m_target;
				listener[0].apply(listener[1], [event]);
			})
		}, 0);
	}
}

export { Emitter }
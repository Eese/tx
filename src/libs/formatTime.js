import { handleNumber } from './errorHandling'

function formatTime(seconds, isAcquarate){
    handleNumber(seconds, 'seconds');
    if(0 == seconds){
        // Early return
        return '00:00:00'
    }
    const SECONDS_PER_DAY = 86400;
    const HOURS_PER_DAY = 24;
    const days = Math.floor(seconds / SECONDS_PER_DAY);
    const remainderSeconds = seconds % SECONDS_PER_DAY;
    let hms = new Date(remainderSeconds * 1000).toISOString().substring(11, 19);
        hms = hms.replace(/^(\d+)/, h => `${Number(h) + days * HOURS_PER_DAY}`.padStart(2, '0'));
        const hoursMinutes = hms.split(':');
        const hours = hoursMinutes[0];
        if(hours == '00') {
            hms = hms.substring(3);
            if('00' == hoursMinutes[1]){
                hms = hms.substring(1);
            }
        }

    if(isAcquarate){
        let decimals = String(seconds).split('.');
            decimals = decimals[1] || '00';
        if(decimals.length < 2){
            decimals+='0'
        }else{
            decimals = decimals.slice(0,2);
        }
        hms = hms+'.'+decimals;
    }
    return hms;
}

export { formatTime }
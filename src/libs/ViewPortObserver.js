'use strict'
import { handleHTML, handleEmptyString, handleArray, handleFunction, handleIfNotNull, handleObject } from './errorHandling'

const ViewPortObserver = function (container, margin, threshold, callcaback, context) {
	handleHTML(container, 		 'container');
	handleEmptyString(margin,	 'margin');
	handleArray(threshold, 	     'threshold');
	handleFunction(callcaback,   'callcaback');
	handleIfNotNull(handleObject, context, 'context');

	const m_container = container;
	const m_margin = margin;
	const m_threshold = threshold;
	const m_callcaback = callcaback;
	const m_context = context;

	let options = {
		root: m_container,
		rootMargin: m_margin,
		threshold: m_threshold
	}

	let entry;
	const m_observer = new IntersectionObserver(function (entryList) {
		const lenght = entryList.length;
		for (let index = 0; index < lenght; index++) {
			entry = entryList[index];
			m_callcaback.apply(m_context, [entry]);
		}
	}, options);

	this.observe = function (content) {
		handleHTML(content,'content');
		m_observer.observe(content);
	}
}

export { ViewPortObserver }
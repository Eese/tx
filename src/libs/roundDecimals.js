function roundDecimals(num, decimals=2){
	return Number(Math.round(num + 'e' + decimals) + 'e-' + decimals);
}

export { roundDecimals }
const MethodData = function(id, method){
    handleEmptyString(id, 'id');
    handleFunction(method, 'method');
    this.id     = id;
    this.method = method;
}

const CallData = function(id, milliseconds, params){
    handleEmptyString(id, 'id');
    handleStringOrNumber(milliseconds, 'milliseconds');
    handleIfNotNull(handleArray, params, 'params');
    this.id           = id;
    this.milliseconds = Number(milliseconds);
    this.params       = params;
}

const CallList = function(){
    const m_list = [];

    this.addItem = function(id, milliseconds, params){
        handleEmptyString(id, 'id');
        handleStringOrNumber(milliseconds, 'milliseconds');
        handleIfNotNull(handleArray, params, 'params');

        const callData = new CallData(id, milliseconds, params);
        m_list.push(callData);
    }

    this.hasMore = function(){
        return 0 < m_list.length
    }

    this.getNext = function(){
        if(m_list.length > 0){
            return m_list.shift();
        }
    }
}

const Kagami = function(name){
    handleEmptyString(name, 'name');

    const RECORD_MODE   = 'recordMode';
    const PLAY_MODE     = 'playMode';
 
    const m_methodList  = {};
    const m_callArray   = [];
    let m_callList      = new CallList(); 

    m_callArray.push(m_callList);

    let m_doPlayTotal   = 0;
    let m_indexCall     = 0;
    let m_mode          = RECORD_MODE;
    let m_lastTime      = Date.now();


    this.addMethod = function(id, method, context){
        handleEmptyString(id, 'id');
        handleFunction(method, 'method');
        handleIfNotNull(handleObject, 'handleObject');

        const now        = Date.now() - m_lastTime;
        const methodData = new MethodData(id, method);
        m_methodList[id] = methodData;
    }

    this.addCall = function(id, params){
        if(PLAY_MODE){
            // Early return
            return
        }
        handleIfNotNull(handleArray, params, 'params');

        const now        = Date.now() - m_lastTime;
        const methodData = m_methodList[id];
        handleObject(methodData, 'methodData');

        m_callList.addItem(id, now, params);
    }

    this.addWaitForEvent = function(){
        if(RECORD_MODE == m_mode){
            m_lastTime = Date.now();
            m_callList = new CallList(); 
            m_callArray.push(m_callList);
        }

        if(PLAY_MODE == m_mode){
            if(m_callList.hasMore()){
                m_doPlayTotal++;
                doPlay();
            }
        }
    }

    this.play = function(dataList){
        handleObject(dataList, 'dataList');
        m_mode = PLAY_MODE;
        let callData;
        let methodData;
        const length = dataList.length;
        for(let index=0; index < length; index++){
            const { id, params } = dataList[index];
            methodData = m_methodList[id];
            handleObject(methodData, 'methodData');
            callData = new CallData(id, method);
        }
    }

    this.getCallList = function(){
        return m_methodListArray;
    }

    /* METHODS */
    function doPlay(){
        if(m_callList.hasMore()){
            const {id, milliseconds, params, context} = m_callList.getNext();
            const {method} = m_methodList[id];
            setTimeout(_=> {
                method.apply(context, params);
                doPlay();
            }, milliseconds);
        }else{
            if(m_doPlayTotal > 0){
                m_doPlayTotal--
                m_callList = m_callArray.shift(); 
                doPlay();
            }
        }
    }
} 
/*
const PathReplicant = function(path){
    const NAME = 'PathReplicant > ';

    const sign = NAME + 'PathReplicant(path) | ';
    handleEmptyString(path, sign + path);

    const m_replicantList = [];

    this.path = path;

    this.addReplicant = function(id, indexList){
        const sign = NAME + 'this.addReplicant(replicant) | ';
        handleObject(replicant, sign + 'replicant');

        handleEmptyString(id, sign + 'id');
        handleArray(indexList, sign + 'indexList');
        const replicant = new Replicant(id, indexList);
        m_replicantList.push(replicant);
    }
}

const MasterReplicant = function(){
    const NAME = 'MasterReplicant > ';
    
    const m_pathReplicant = [];
    let m_currentPathReplicant;

    this.addReplicant = function(id, indexList){
        const sign = NAME + 'this.addReplicant(replicant) |';
        handleEmptyString(id, sign + 'id');
        handleArray(indexList, sign + 'indexList');

        m_currentPathReplicant.addReplicant(id, indexList);
    }

    this.addPath = function(path){
        m_currentPathReplicant = new PathReplicant(path);
    }
}

let masterRecorder;
if(null == window.masterRecorder){
    masterRecorder = new MasterReplicant();
    window.masterRecorder = masterRecorder;
}else{
    masterRecorder = window.masterRecorder;
}
*/
export { Kagami }
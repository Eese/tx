function Logger(prefix = ''){
    const _accumulationList = [];

    this.add = function (...params){
        _accumulationList.push([prefix,...params]);
    }

    this.addStringify = function(object){
        _accumulationList.push([prefix, JSON.stringify(object, null, 4)]);
    }

    this.show = function(shower){
        _accumulationList.forEach((params)=>{
            shower.apply(null, params);
        });
        _accumulationList.length = 0;
    }

    this.clear = function(){
        _accumulationList.length = 0;
    }
}

window.getLogger = function(prefix){
    return new Logger(prefix);
}

export {Logger}

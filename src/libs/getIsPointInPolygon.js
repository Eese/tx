function getIsPointInPolygon(point, polygon) {
    const [px, py] = point;
    let inside = false;
    for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
        const [xi, yi] = polygon[i];
        const [xj, yj] = polygon[j];

        const intersect = ((yi > py) !== (yj > py)) &&
                          (px < (xj - xi) * (py - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }
    return inside;
}

// Example usage:
const polygon = [
    [0, 0], [5, 0], [5, 5], [0, 5], [2, 8]
];
const point1 = [3, 3]; // Inside point
const point2 = [6, 6]; // Outside point

export { getIsPointInPolygon }
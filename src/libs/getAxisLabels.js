import { handleArray, handleIfNotNull, handleNumber } from './errorHandling'

const DEFAULT_MULTIPLE = 10;

let getHyperRound = function (minValue, maxValue, steps, multiple, isTracer) {
    const diff = maxValue-minValue;
    let fractionNumber = Math.floor(diff/steps);
    const fraction = String(fractionNumber);
    let firstDigit = Number(String(fraction).charAt(0));
    let secondsDigit = Number(String(fraction).charAt(1));
    let digit = firstDigit+''+secondsDigit;
    if (null != secondsDigit) {
        if(secondsDigit <  5 && secondsDigit > 0){
            digit = firstDigit + '5';
        } else if(secondsDigit > 5){
            digit = String(firstDigit + 1)+secondsDigit;
        }
        const length = fraction.length - 2;
        for (let index = 0; index < length; index++) {
            digit = digit + '0';
        }
    }
    const finalFraction = Number(digit);
    
    return finalFraction
}

let getFraction = function (minValue, maxValue, steps, multiple, isTracer) {
    const sign = 'getFraction(minValue, maxValue, steps) | ';
    
    handleNumber(minValue, sign + 'minValue');
    handleNumber(maxValue, sign + 'maxValue');
    handleNumber(steps, sign    + 'steps');
    handleIfNotNull(handleNumber, multiple, sign + 'multiple');
    
    const length = Math.max(String(Math.round(minValue)).length, String(Math.round(maxValue)).length);
    let finalMinValue = Math.min(minValue, maxValue);
    let finalMaxValue = Math.max(minValue, maxValue);
    let finalMultiple = multiple || DEFAULT_MULTIPLE;
    
    let finalFraction;
        finalFraction = getHyperRound(finalMinValue, finalMaxValue, steps, finalMultiple, isTracer);
        finalMaxValue = finalFraction*steps;
    
    if (finalMinValue < 0) {
        let negativeSteps = Math.floor(finalFraction / (-finalMinValue));
            negativeSteps = Math.max(1, negativeSteps);
        const fractionData = getHyperRound(0, -finalMinValue, negativeSteps, finalMultiple);
        finalMinValue = -(fractionData.fraction * negativeSteps);
        finalFraction = getHyperRound(finalMinValue, finalMaxValue, steps, finalMultiple);
    }
    
    if (finalMaxValue < 0) {
        let negativeSteps = Math.floor(finalFraction / (-finalMaxValue));
            negativeSteps = Math.max(1, negativeSteps);
        const fractionData = getHyperRound(0, -finalMaxValue, negativeSteps, finalMultiple);
        finalMaxValue = -(fractionData.fraction * negativeSteps);
        finalFraction = getHyperRound(finalMinValue, finalMaxValue, steps, finalMultiple);
    }
    const list = [];
    for (let index = 0; index < steps+1; index++){
        list.push(index * finalFraction);
    }

    const data = {
                    fraction:   finalFraction,
                    minValue:   finalMinValue,
                    maxValue:   finalMaxValue,
                    list:       list.reverse()
                }
    return data
}

const getLabelsFromList = function (list, steps, isTracer) {
    const sign = 'getLabelsFromList(list, steps) | ';
    
    handleArray(list, sign + 'list');
    handleNumber(steps, sign + 'steps');
    
    let length = list.length;
    let minValue = 0;
    let maxValue = 0;
    let value;
    for (let index = 0; index < length; index++){
        value = list[index];
        if (null === value) {
            continue
        }
        handleNumber(value, sign + 'list[' + index + ']');
        minValue = Math.min(value, minValue);
        maxValue = Math.max(value, maxValue);
    }
    const data = getFraction(minValue, maxValue, steps, null, isTracer);
    
    return data
}

export { getLabelsFromList }
import { Emitter } from '../events/Emitter.js'

var counter = 0;

function FileBrowser(input, dropArea, callback, context) {
	handleObject(input, 'input');
	handleObject(dropArea, 'dropArea');
	handleFunction(dropArea.addEventListener, 'dropArea.addEventListener');

	if (null == callback) {
		callback = function () { };
	}

	handleFunction(callback, 'callback');
	handleIfNotNull(handleObject, context, 'handleObject');

	const _input = input;
	const _inputId = _input.id != '' || null == _input.id ? _input.id : 'ReNomImageUploader_' + (counter);
	const _callback = callback;
	const _context = context;
	const _dropArea = dropArea;

	counter++;

	const _emitter = new Emitter(this, ['loadstart', 'load', 'progress', 'error', 'abort', 'browseCancel', 'over', 'out', 'drop']);

	_dropArea.addEventListener('dragenter', onDropEvent, false);
	_dropArea.addEventListener('dragover',  onDropEvent, false);
	_dropArea.addEventListener('dragleave', onDropEvent, false);
	_dropArea.addEventListener('drop',      onDropEvent, false);
	_input.addEventListener(   'change',    onBrowse);

	const eventDictionay = {
		dragenter: 'over',
		dragover: 'over',
		dragleave: 'out',
		drop: 'drop'
	}

	function refreshInput() {
		const input = document.getElementById(_inputId);
		if (null != input) {
			const parent = input.parentElement;
			if (null != parent) {
				document.removeChild(input);
			}
			parent.appendChild(input);
		}
	}

	function onDropEvent(event) {
		handleObject(event, 'event');
		event.preventDefault(event);
		event.stopPropagation(event);
		const eventToEmit = eventDictionay[event.type];
		emitEvent(eventToEmit, event);
		callback.apply(_context, [event]);
		if ('drop' == eventToEmit) {
			handleFiles(event.dataTransfer.files);
		}
	}

	function onBrowse(event) {
		handleObject(event,              'event');
		handleObject(event.target,       'event.target');
		handleObject(event.target.files, 'event.target.files');
		handleFiles(event.target.files);
	}

	function handleFiles(files) {
		handleObject(files, 'files');
		files = [...files];
		if (0 == files.length) {
			emitEvent('browseCancel');
			return
		}
		const _self = this;
		files.forEach(function (file) {
			let reader = new FileReader();

			reader.onabort = function () {
				emitEvent('abort');
			}
			reader.onloadstart = function () {
				emitEvent('loadstart');
			}
			reader.onload = function (event) {
				emitEvent('load', reader.result, file);
			}

			reader.onerror = function (event) {
				throw new Error('FILE PREVIEW ERROR', event);
			}

			reader.readAsDataURL(file);
		})
	}

	this.addEventListener = function (type, methodToAdd, contextToAdd) {
		_emitter.addEventListener(type, methodToAdd, contextToAdd);
	}

	this.removeEventListener = function (type, methodToAdd, contextToAdd) {
		_emitter.removeEventListener(type, methodToAdd, contextToAdd);
	}

	function emitEvent(type, url, file) {
		const event = {
			type,
			url,
			file
		}
		_emitter.emitEvent(type, event);

		_callback.apply(_context, [{
			type,
			url,
			file
		}]);
	}
}

export { FileBrowser }
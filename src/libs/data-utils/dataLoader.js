const defaultHeaders = {}
defaultHeaders['Content-Type']= 'application/json';

function getErrorDescription (body, headers, response){
    let errorDescription =  `\n---------------------\n`;
        errorDescription += `Error loading data\n`;
        errorDescription += `url: ${response.url}\n`;
        errorDescription += `status: ${response.status}\n`;
        errorDescription += `statusText: ${response.statusText}\n`;
        errorDescription += `\ntype: ${response.type}\n`;
        errorDescription += `\headers:\n ${JSON.stringify(headers, null, 4)}\n`
        errorDescription += `\nbody:\n ${JSON.stringify(body, null, 4)}\n`
        errorDescription += `---------------------\n`;
    return errorDescription
}

function getErrorData (body, headers, response){
    return {
        url:response.url,
        headers,
        body,
        type: response.type,
        status: response.status,
        statusText: response.statusText
    }
}

function getOptions(body, additionalHeaders, method){
    handleNotValidString(method, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']);
    const headers     = { ...defaultHeaders, ...additionalHeaders }
    const optionsData = {
        headers,
        method,
        credentials: 'include'
    }
    if(null != body){
        optionsData.body = JSON.stringify(body);
    }
    return optionsData
}

async function call(url, body, headers, method, errorHandler){
    const options = getOptions(body, headers, method);
    const response = await fetch(url, options);
    if (response.ok) {
        const data = await response.json();
        return {data}
    } else {
        if(null != errorHandler){
            errorHandler(getErrorData (body, headers, response));
        }else{
            throw new Error(getErrorDescription(body, headers,response));
        }
    }
}

async function callGET(url, headers={}, errorHandler){
    handleEmptyString(url, 'url')
    handleDataStructure(headers, 'headers');
    handleIfNotNull(handleFunction, errorHandler, 'errorHandler');

    const method = 'GET'
    const response = await call(url, null, headers, method, errorHandler);
    return response
}

async function callPOST(url, body={}, headers={}, errorHandler){
    handleEmptyString(url, 'url')
    handleDataStructure(body, 'body');
    handleDataStructure(headers, 'headers');
    handleIfNotNull(handleFunction, errorHandler, 'errorHandler')

    const method = 'POST'
    const response = await call(url, body, headers, method, errorHandler);
    return response
}

async function callPUT(url, body={}, headers={}, errorHandler){
    handleEmptyString(url, 'url')
    handleDataStructure(body, 'body');
    handleDataStructure(headers, 'headers');
    handleIfNotNull(handleFunction, errorHandler, 'errorHandler')

    const method = 'PUT'
    const response = await call(url, body, headers, method, errorHandler);
    return response
}

async function callPATCH(url, body={}, headers={}, errorHandler){
    handleEmptyString(url, 'url')
    handleDataStructure(body, 'body');
    handleDataStructure(headers, 'headers');
    handleIfNotNull(handleFunction, errorHandler, 'errorHandler')

    const method = 'PATCH'
    const response = await call(url, body, headers, method, errorHandler);
    return response
}

async function callDELETE(url, body={}, headers={}, errorHandler){
    handleEmptyString(url, 'url')
    handleDataStructure(body, 'body');
    handleDataStructure(headers, 'headers');
    handleIfNotNull(handleFunction, errorHandler, 'errorHandler')

    const method = 'DELETE'
    const response = await call(url, body, headers, method, errorHandler);
    return response
}

export {
    callGET,
    callPOST,
    callPUT,
    callPATCH,
    callDELETE
}
'use strict'
const getRandomId = function () {

	return Math.random().toString(36).substr(2, 8).toUpperCase() + '-' + Math.random().toString(36).substr(2, 8).toUpperCase();
}

const getTimeId = function () {
	var today = new Date().toLocaleDateString('ja-JP-u-ca-japanese', {
		day: 'numeric',
		month: 'short',
		year: 'numeric'
	})
	var currentDate = new Date();
	var hours = currentDate.getHours();
	var minutes = currentDate.getMinutes();
	return (hours + ':' + minutes + '-' + (Date.now().toString(36) + '-' + Math.random().toString(36).substr(2, 8)).toUpperCase());
}

export { getRandomId, getTimeId }
'use stric'
import { handleHTML, hhandleNumber } from '../../errors/errorHandling'
import { Emitter } from '../../events/Emitter'

const MOUSE_DOWN   = 'mousedown';
const MOUSE_UP     = 'mouseup';
const MOUSE_MOVE   = 'mousemove';
const MOUSE_LEAVE  = 'mouseleave'

const TOUCH_START  = 'touchstart';
const TOUCH_MOVE   = 'touchmove';
const TOUCH_END    = 'touchend';
const TOUCH_CANCEL = 'touchcancel';

'use strict'

const MouseSlider = function(track, axis) {
    const sign = 'MouseSlider(track) | ';
    handleHTML(track, sign + 'track');

    let m_self = this;

    const m_track = track;

    let m_emitter    = new Emitter(this, [MOUSE_UP, MOUSE_DOWN, MOUSE_MOVE]);
    let m_isDraggingX = false;

    let m_minRatioX = 0;
    let m_maxRatioX = 1;
    let m_ratioX    = 0;

    let m_positionX = 0;
    let m_positionY = 0;

    let m_minX = null;
    let m_maxX = null;
    let m_minY = null;
    let m_maxY = null;

    let m_willEmitMouseDown = false;
    let m_time              = null;
    let m_lastMoveX         = null;

    //TOUCH_CANCEL
    m_track.addEventListener(MOUSE_DOWN, onMouseDown);
    m_track.addEventListener(TOUCH_START, onMouseDown);
    
    m_track.addEventListener(MOUSE_UP, onMouseUp);
    m_track.addEventListener(TOUCH_END, onMouseUp);
    
    document.addEventListener(MOUSE_UP, onMouseUp);
    document.addEventListener(TOUCH_END, onMouseUp);
    document.addEventListener(MOUSE_LEAVE, onMouseUp);
    document.addEventListener(TOUCH_CANCEL, onMouseUp);
    
    window.addEventListener(MOUSE_UP, onMouseUp);
    window.addEventListener(TOUCH_END, onMouseUp);
    window.addEventListener(MOUSE_LEAVE, onMouseUp);
    window.addEventListener(TOUCH_CANCEL, onMouseUp);
    
    window.addEventListener(MOUSE_MOVE, onMouseMove);
    window.addEventListener(TOUCH_MOVE, onMouseMove);
    
    this.setMinMaxRatioX = function(minRatio, maxRatio){
        handleNumber(minRatio, 'minRatio');
        handleNumber(maxRatio, 'maxRatio');
        let finalMinRatio = Math.min(Math.max(minRatio, 0), 1);
        let finalMaxRatio = Math.min(Math.max(maxRatio, 0), 1);

        finalMinRatio = Math.min(finalMinRatio, finalMaxRatio);
        finalMaxRatio = Math.max(finalMinRatio, finalMaxRatio);

        m_minRatioX = finalMinRatio;
        m_maxRatioX = finalMaxRatio;
    }

    this.setRatioX = function(ratio){
        handleNumber(ratio, 'ratio');
        let finalRatio = Math.max(ratio, m_minRatioX);
            finalRatio = Math.min(finalRatio, m_maxRatioX);

        setMinMax();

        m_ratioX = finalRatio;
        setPositionByRatioX(m_ratioX);
    }

    this.getRatioX = function(){
        return m_ratioX;
    }

    this.setPositionX = function(position){
        handleNumber(position, 'position');

        setMinMax();

        m_positionX = Math.min(Math.max(position, m_minX), m_maxX);
        setRatioByPositionX(m_positionX);
    }

    this.getPositionX = function(){
        return m_positionX;
    }

    function getClientX(event){
        let clientX = event.clientX;
        if(null != event.touches){
            const touch = event.touches[0]
            clientX = touch.clientX;
        }
        return clientX
    }

    /* METHODS */
    function setMinMax(){
        const boundingBox = m_track.getBoundingClientRect();
        m_minX = boundingBox.x;
        m_minY = boundingBox.y;
        m_maxX = m_track.scrollWidth;
        m_maxY = m_track.scrollHeight;
    }

    function setPositionByRatioX(ratio){
        handleNumber(ratio, 'ratio');

        setMinMax();

        m_ratioX = Math.min(Math.max(ratio, 0), 1);
        m_positionX = m_ratioX * m_maxX;
    }

    function setRatioByPositionX(position){
        handleNumber(position, 'position');
        setMinMax();
        m_positionX = Math.min(Math.max(position, 0), m_maxX);
        m_ratioX = m_positionX / m_maxX;
    }
    
    /* EVENTS */
    function wheelDistance(event) {
        if (!event) {
            event= window.event;
        }
         let w = event.wheelDelta,
             d = event.detail;
         if (d) {
             return -d / 3; // Firefox;
         }
       
         // IE, Safari, Chrome & other browsers
         return w / 120;
     }

    function onMouseDown(event) {
        event.preventDefault();
        m_time = event.timeStamp;
        m_willEmitMouseDown = true;

        const mouseX = getClientX(event);
        setMinMax();
        const positionX = mouseX - m_minX;

        setRatioByPositionX(positionX);

        m_lastMoveX = mouseX;
        m_isDraggingX = true;
        
        emit(MOUSE_DOWN);
    }

    function onMouseUp(event) {
        m_isDraggingX = false;

        const mouseX = getClientX(event);
        setMinMax();
        const positionX = mouseX - m_minX;
        setRatioByPositionX(positionX);

        if(m_willEmitMouseDown){
            emit(MOUSE_UP);
        }
        m_willEmitMouseDown = false;
    }

    function onMouseMove(event) {
        if (!m_isDraggingX || 0 == event.movementX) {
            // Early return
            return
        }
        event.preventDefault();
        
        const mouseX = getClientX(event);
        setMinMax();
        const positionX = mouseX - m_minX;
        setRatioByPositionX(positionX);

        m_time = Date.now();
        m_lastMoveX = mouseX;

        emit(MOUSE_MOVE);
    }

    this.addEventListener = function(type, listener, context){
        m_emitter.addEventListener(type, listener, context);
    }

    this.addAllEventListeners = function(listener, context){
        m_emitter.addAllEventListeners(listener, context);
    }

    this.removeEventListener = function(type, listener, context){
        m_emitter.removeEventListener(type, listener, context);
    }

    function emit(type){
        const event = {
            ratioX        : m_ratioX,
            isDraggingX   : m_isDraggingX,
            positionX     : m_positionX
        }
        m_emitter.emitEvent(type, event);
    }
}

export { MouseSlider }
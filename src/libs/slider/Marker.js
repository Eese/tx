'use stric'
import { handleHTML, handleNumber } from '../errorHandling'
import { Emitter } from '../events/Emitter'

const MOUSE_DOWN = 'mousedown';
const MOUSE_UP = 'mouseup';
const MOUSE_MOVE = 'mousemove';
const MOUSE_LEAVE = 'mouseleave'

'use strict'
const TOUCH_START = 'touchstart';
const TOUCH_MOVE = 'touchmove';
const TOUCH_END = 'touchend';
const TOUCH_CANCEL = 'touchcancel';

const Marker = function(button, minPosition, maxWidth, isNotInteractive) {
    handleHTML(button, 'button');
    handleNumber(minPosition, 'minPosition');
    handleNumber(maxWidth, 'maxWidth');

    let m_self = this;
    let m_emitter = new Emitter(this, [MOUSE_UP, MOUSE_DOWN, MOUSE_MOVE]);
    let m_isDragging = false;

    let m_minRatio = 0;
    let m_maxRatio = 1;
    
    let m_minPosition = minPosition;
    let m_maxWidth = maxWidth;
    let m_buttonOffset = 0;

    let m_ratio = 0;

    let m_willEmitMouseDown = false;
    let m_time = null;
    let m_lastMove = null;
    


    //TOUCH_CANCEL
    if(!isNotInteractive){

        button.addEventListener(MOUSE_DOWN, onMouseDown);
        button.addEventListener(TOUCH_START, onMouseDown);
        
        button.addEventListener(MOUSE_UP, onMouseUp);
        button.addEventListener(TOUCH_END, onMouseUp);
        
        document.addEventListener(MOUSE_UP, onMouseUp);
        document.addEventListener(TOUCH_END, onMouseUp);
        document.addEventListener(MOUSE_LEAVE, onMouseUp);
        document.addEventListener(TOUCH_CANCEL, onMouseUp);
        
        window.addEventListener(MOUSE_UP, onMouseUp);
        window.addEventListener(TOUCH_END, onMouseUp);
        window.addEventListener(MOUSE_LEAVE, onMouseUp);
        window.addEventListener(TOUCH_CANCEL, onMouseUp);
        
        window.addEventListener(MOUSE_MOVE, onMouseMove);
        window.addEventListener(TOUCH_MOVE, onMouseMove);
    }
    
    this.setMinMaxRatio = function(minRatio, maxRatio, name){
        handleNumber(minRatio, 'minRatio');
        handleNumber(maxRatio, 'maxRatio');
        let finalMinRatio = Math.min(Math.max(minRatio, 0), 1);
        let finalMaxRatio = Math.min(Math.max(maxRatio, 0), 1);
        
        finalMinRatio = Math.min(finalMinRatio, finalMaxRatio);
        finalMaxRatio = Math.max(finalMinRatio, finalMaxRatio);

        m_minRatio = finalMinRatio;
        m_maxRatio = finalMaxRatio;
    }

    this.setRatio = function(ratio){
        handleNumber(ratio, 'ratio');
        let finalRatio = Math.max(ratio, m_minRatio);
            finalRatio = Math.min(finalRatio, m_maxRatio);
        const offset = (finalRatio*m_maxWidth)+m_minPosition;
        m_ratio = finalRatio;
        setOffest(offset);
    }

    this.getRatio = function(){
        return m_ratio;
    }
    
    this.updateSize = function(minPosition, maxWidth){

        const sign = 'updateSize(minPosition, maxWidth) | ';
        handleNumber(minPosition, 'minPosition');
        handleNumber(maxWidth, 'maxWidth');
        m_minPosition = minPosition;
        m_maxWidth = maxWidth;
        this.setRatio(m_ratio);
    }

    this.addEventListener = function(type, listener, context){
        m_emitter.addEventListener(type, listener, context);
    }

    this.removeEventListener = function(type, listener, context){
        m_emitter.removeEventListener(type, listener, context);
    }

    function emit(type, data){
        m_emitter.emitEvent(type, data);
    }

    function onMouseDown(event) {
        event.preventDefault();
        m_time = event.timeStamp;
        m_willEmitMouseDown = true;
        const clientX = getClientX(event);

        m_lastMove = clientX;
        m_buttonOffset = clientX - button.offsetLeft;
        m_isDragging = true;
        emit(MOUSE_DOWN, {ratio:m_ratio, isDragging: true});
    }

    function onMouseUp(event) {
        m_isDragging = false;

        if(m_willEmitMouseDown){
            emit(MOUSE_UP, {ratio:m_ratio, isDragging: false});
        }
        m_willEmitMouseDown = false;
    }

    function onMouseMove(event) {
        if (!m_isDragging || 0 == event.movementX) {
            // Early return
            return
        }
        event.preventDefault();
        const clientX = getClientX(event);
        
        m_time = Date.now();
        m_lastMove = clientX;
        
        let offset = clientX - /*trackOffsetLeft - */ m_buttonOffset;
        let ratio = (offset-m_minPosition)/m_maxWidth;
            ratio = Math.min(Math.max(ratio, m_minRatio), m_maxRatio);
            offset = (ratio*m_maxWidth)+m_minPosition;

        m_ratio = ratio;

        setOffest(offset);
        emit(MOUSE_MOVE, {ratio:m_ratio, isDragging: true});
    }

    function setOffest(offset){
        handleNumber(offset, 'offeset');
        button.style.left = (offset/16) + "rem";
    }

    function getClientX(event){
        let clientX = event.clientX;
        if(null != event.touches){
            const touch = event.touches[0]
            clientX = touch.clientX;
        }
        return clientX
    }
}

export { Marker }
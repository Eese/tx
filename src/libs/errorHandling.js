const HANDLED_ERROR = '\n\nCUSTOM HANDLED ERROR:\n';
const CUSTOM_DETAILS = 'More details of the error:\n';

const checkIsNumber = function (number) {
	const type = typeof (number);
	if (null == number || type != 'number' || isNaN(number * 1) || type == 'number' && isNaN(number)) {
		return false;
	}
	return true;
}

const checkIsStringNumber = function (stringNumber) {
	if (null == stringNumber) {
		//Early return
		return false;
	}
	
	const number = Number(stringNumber);
	const resultIsNumber = checkIsNumber(number);
	return resultIsNumber
}

const checkIsNaN = function (number, isDebugger) {
	return !checkIsNumber(number);
}

const checkIsArray = function (list) {
	return Array.isArray(list);
}

const checkIsObject = function (object) {
	return (typeof object === "object") && (object !== null);
}

const checkIsDataScructure= function (object) {
	return (typeof object === "object") && (object !== null) && !(Array.isArray(object));
}

const checkIsHTMLElement = function (element) {
	if (null == element || !checkIsObject(element)) {
		// Early return
		return false;
	}

	if (element instanceof Node) {
		const isNode = null != element &&
			typeof element == "object" &&
			typeof element.nodeType == "number" &&
			typeof element.nodeName == "string";
		if (isNode) {
			// Early return
			return true
		}
	}

	if (element instanceof HTMLElement) {
		// Early return
		return true
	} else {
		const isHTMLElement = null != element &&
			typeof element == "object" &&
			element.nodeType === 1 &&
			typeof element.nodeName == "string"
		if (isHTMLElement) {
			// Early return
			return true
		}
	}

	return false;
}

function getErrorMessage(message){
	return `\n ${CUSTOM_DETAILS}\n${null == message ? '' : message}`;
}

function formatExpectedError(variableName, expectedText, value, message){
	let description =  `CUSTOM_DETAILS\n`;
		description += `Unexpected variable detected:\n`;
		if(undefined != variableName){
			description += `• variable name: ${variableName}\n`;
		}
		description += `• expected: ${expectedText}\n`;
		description += `----------------------------\n`;
		description += `Actualy got:\n`;
		description += `• value: ${value}\n`;
		const typeofValue = Array.isArray(value) ? 'Array' : typeof value;
		description += `• type:  ${typeofValue}\n`;
		description += null == message ? '' : `\nMore details:\n${message}`;

	return `${HANDLED_ERROR} ${description}\n\nCALL STACK:`
}

const handleArray = function (array, variableName, description) {
	if (null == array) {
		const errorDescription = formatExpectedError(variableName, 'Array', array, description);
		throw new Error( errorDescription );
	}
	if (!Array.isArray(array)) {
		const errorDescription = formatExpectedError(variableName, 'Array', array, description);
		throw new Error( errorDescription );
	}
	return array
}

const handleBoolean = function (boolean, variableName, description) {
	if (boolean !== false && boolean !== true) {
		const errorDescription = formatExpectedError(variableName, 'Boolean', boolean, description);
		throw new Error( errorDescription );
	}

	return boolean
}

const handleObject = function (object, variableName, description) {
	if (null == object) {
		const errorDescription = formatExpectedError(variableName, 'Object', object, description);
		throw new Error(errorDescription);
	}

	if (typeof object != 'object') {
		const errorDescription = formatExpectedError(variableName, 'Object', object, description);
		throw new Error(errorDescription);
	}
	return object
}

const handleDataStructure = function (object, variableName, description) {
	//return (typeof object === "object") && (object !== null) && !(Array.isArray(object));
	const isNotDataScructure = !checkIsDataScructure(object);
	if(isNotDataScructure){
		const errorDescription = formatExpectedError(variableName, 'Object', object, description);
		throw new Error(errorDescription);
	}
	return object
}

const handleNotValidString = function (value, list, variableName, description) {
	handleArray(list, 'list');
	let length     = list.length;
	const iterator = list.entries();
	for (const [index, word] of iterator) {
		if (word == value) {
			// Early return
			return value;
		}
	}
	const errorDescription = formatExpectedError(variableName, `one of the follows values: ${list}`, value, description);
	throw new Error( errorDescription );
}

const handleNumber = function (numberTocheck, variableName, description) {
	if (checkIsNaN(numberTocheck)) {
		const errorDescription = formatExpectedError(variableName, `Number`, numberTocheck, description);
		throw new Error( errorDescription );
	}

	return numberTocheck
}

const handleStringNumber = function (stringNumber, variableName, description) {
	if (!checkIsStringNumber(stringNumber)) {
		const errorDescription = formatExpectedError(variableName, `String castable to Number or Number`, stringNumber, description);
		throw new Error( errorDescription );
	}

	return stringNumber
}

const handleString = function (value, variableName, description) {
	if (null == value) {
		const errorDescription = formatExpectedError(variableName, `String`, value, description);
		throw new Error( errorDescription );
	}

	if (typeof value != 'string') {
		const errorDescription = formatExpectedError(variableName, `String`, value, description);
		throw new Error( errorDescription );
	}

	return value
}

const handleNumberWithUnit = function (value, variableName, description) {
	const errorMessage = formatExpectedError(variableName, `Number with unit`, value, description);
	if(checkIsNaN(parseFloat(value))){
		throw new Error(errorMessage);
	}

	return value
}

const handleEmptyString = function (value, variableName, description) {
	const errorMessage = getErrorMessage('handleEmptyString from errorHandling.js ' + description);

	handleString(value, variableName,  + errorMessage);

	if (0 == value.length) {
		const errorDescription = formatExpectedError(variableName, `non-empty String`, value, description);
		throw new Error( errorDescription );
	}

	return value
}

const handleNull = function (value, variableName, description) {
	const errorMessage = getErrorMessage(description);

	if (null == value) {
		throw new Error(`The value must not be null ${errorMessage}`);
	}

	return value
}

const handleStringOrNumber = function (value, variableName, description) {
	if (checkIsNumber(value)) {
		// Early return
		return;
	}

	if (null == value || typeof value != 'string') {
		const errorDescription = formatExpectedError(variableName, `Number or String`, value, description);
		throw new Error(  errorDescription );
	}

	return value
}

const handleFunction = function (functionRef, variableName, description) {
	if (typeof functionRef != 'function') {
		const errorDescription = formatExpectedError(variableName, 'function', functionRef, description);
		throw new Error(errorDescription);
	}

	return functionRef
}

const handleHTML = function (element, variableName, description) {
	if (!checkIsHTMLElement(element)) {
		const errorDescription = formatExpectedError(variableName, `HTMLElement`, element, description);
		throw new Error(errorDescription);
	}

	return element
}

const handleIfNotNull = function (handler, value, variableName, description) {
	if(typeof handler != 'function'){
		throw new Error('errorHandling handleIfNotNull, the handler must be a function.');
	}
	if (null != value && undefined != value) {
		handler(value, variableName, description);
	}

	return value
}

const handleInstanceOf = function (value, classObject, variableName, description) {
	const isNotDescendant = !(value instanceof classObject);
	if (isNotDescendant) {
		const errorDescription = formatExpectedError(variableName, `instance of ${classObject}`, value, description);
		throw new Error(errorDescription);
	}

	return value
}

const handleTime = function(timeToTest, variableName, description){
	if(null == timeToTest){
		const errorDescription = formatExpectedError(variableName, 'String or Number, not null', timeToTest, description);
		throw new Error(errorDescription);
	}
	const isStringOrNumber = checkIsNumber(timeToTest) || checkIsStringNumber(timeToTest) || (typeof timeToTest == 'string');
	if(!isStringOrNumber){
		const errorDescription = formatExpectedError(variableName, 'String or Number for valid time', timeToTest, description);
		throw new Error(errorDescription);
	}
	
	const date = new Date(timeToTest);
	const time = date.getTime();
	if(isNaN(time)){
		const errorDescription = formatExpectedError(variableName, 'Valid time', timeToTest, description);
		throw new Error(errorDescription);
	}

	return timeToTest
}

window.handleArray          = handleArray;
window.handleObject         = handleObject;
window.handleNotValidString = handleNotValidString;
window.handleNumber         = handleNumber;
window.handleNumberWithUnit = handleNumberWithUnit;
window.handleStringNumber   = handleStringNumber;
window.handleString         = handleString;
window.handleEmptyString    = handleEmptyString;
window.handleNull 		    = handleNull;
window.handleStringOrNumber = handleStringOrNumber;
window.handleFunction 	    = handleFunction;
window.handleHTML 		    = handleHTML;
window.handleIfNotNull      = handleIfNotNull;
window.handleInstanceOf     = handleInstanceOf;
window.handleBoolean 	    = handleBoolean;
window.handleTime 		    = handleTime;
window.handleDataStructure  = handleDataStructure;

export {
	checkIsNumber,
	checkIsNaN,
	checkIsArray,
	checkIsHTMLElement,
	checkIsObject,
	checkIsStringNumber,
	checkIsDataScructure,

	handleArray,
	handleObject,
	handleNotValidString,
	handleNumber,
	handleNumberWithUnit,
	handleStringNumber,
	handleString,
	handleEmptyString,
	handleNull,
	handleStringOrNumber,
	handleFunction,
	handleHTML,
	handleIfNotNull,
	handleInstanceOf,
	handleBoolean,
	handleTime,
	handleDataStructure
}
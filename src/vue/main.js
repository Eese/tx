import { createApp } from 'vue'
import App from './app.vue'
import router from './router/router'

import { toString } from '../libs/toString.js'
window.toString = toString;

import { clone } from '../libs/clone.js'
window.clone    = clone;

import { roundDecimals } from '../libs/roundDecimals.js'
window.roundDecimals = roundDecimals;

import { isDeepEqual } from '../libs/isDeepEqual.js'
window.isDeepEqual = isDeepEqual;

import { localize } from './__stores__/Lang/state__Lang.js'
window.localize = localize;

import { handleArray, handleObject, handleNotValidString, handleNumber, handleNumberWithUnit, handleStringNumber, 
    handleString, handleEmptyString, handleNull, handleStringOrNumber, handleFunction, handleHTML, handleIfNotNull, 
    handleInstanceOf, handleBoolean, handleTime, handleDataStructure, checkIsNumber, checkIsNaN, checkIsArray, 
    checkIsHTMLElement, checkIsObject, checkIsStringNumber, checkIsDataScructure }  from '../libs/errorHandling.js'


window.handleArray          = handleArray;
window.handleObject         = handleObject;
window.handleNotValidString = handleNotValidString;
window.handleNumber         = handleNumber;
window.handleNumberWithUnit = handleNumberWithUnit;
window.handleStringNumber   = handleStringNumber;
window.handleString         = handleString;
window.handleEmptyString    = handleEmptyString;
window.handleNull           = handleNull;
window.handleStringOrNumber = handleStringOrNumber;
window.handleFunction       = handleFunction;
window.handleHTML           = handleHTML;
window.handleIfNotNull      = handleIfNotNull;
window.handleInstanceOf     = handleInstanceOf;
window.handleBoolean        = handleBoolean;
window.handleTime           = handleTime;
window.handleDataStructure  = handleDataStructure;

window.checkIsNumber        = checkIsNumber;
window.checkIsNaN           = checkIsNaN;
window.checkIsArray         = checkIsArray;
window.checkIsHTMLElement   = checkIsHTMLElement;
window.checkIsObject        = checkIsObject;
window.checkIsStringNumber  = checkIsStringNumber;
window.checkIsDataScructure = checkIsDataScructure;

import { Logger } from '../libs/acumulator.js';

// See doc in the file.
import { activatePasiveListener } from '../libs/events/activatePasiveListener'
activatePasiveListener();

const app = createApp(App);

app.use(router);
app.mount('#main-app');
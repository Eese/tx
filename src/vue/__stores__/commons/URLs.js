const PROD_API = 'https://func-analytic-prod-jpeast-001.azurewebsites.net/api/';
const PROT_API = 'https://func-analytic-prot-jpeast-001.azurewebsites.net/api/';
const URL_API = PROT_API;
const COMPARISON_API = URL_API + 'get_store_comparison_kpi';

const APPWRITE_URL = 'https://675e2dd830c4a2011e37.appwrite.global/';

const featureInterop = window.featureInterop;
//getCurrentBackend

export {
    URL_API,
    COMPARISON_API,
    APPWRITE_URL
}
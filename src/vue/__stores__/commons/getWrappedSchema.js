import { getSchemaObject} from '../../../libs/data-utils/mapBySchema.js'

function getWrappedSchema(schemaChildren){
    handleDataStructure(schemaChildren, 'schemaChildren');
    const innerData = getSchemaObject(schemaChildren,   {skip:true});
    const result    = getSchemaObject({data:innerData}, {skip:true});
    const data      = getSchemaObject({result},         {skip:true});
    const root      = getSchemaObject({data},           {skip:true});
    return root
}

export { getWrappedSchema }
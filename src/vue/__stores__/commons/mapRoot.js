import { handleDataStructure } from "../../../libs/errorHandling"

function mapRoot(rootData){
    handleDataStructure(rootData, 'rootData');
    const { data:response } = rootData;
    handleDataStructure(response, 'response');
    const { result } = response;
    handleDataStructure(result, 'result');
    let { data } = result;
    return data
}

function mapRootAPI(rootData){
    const {data}   = rootData;
    const {result} = data;
    return result
}

export {  mapRoot, mapRootAPI }
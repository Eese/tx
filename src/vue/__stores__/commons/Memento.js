function Memento (state){
    handleDataStructure(state, 'state')
    const _state        = state;

    const _stateMemento = [];
    let _mementoIndex   = 0;
    let _timeStamp      = Date.now();
    let _isPlaying      = false;
    let _delayId        = null;

    _stateMemento.push({
        props: clone(state),
        delay: 3500
    })

    // HELPERS
    function doPlay(){
        if(_mementoIndex >= _stateMemento.length || _mementoIndex < 0){
            _mementoIndex = 0;
            return
        }
        
        const delay = changeState();
        _mementoIndex++


        _delayId = setTimeout(()=>{
            doPlay();
        }, Math.min(delay, 3000));
    }

    function changeState(){
        const memento = _stateMemento[_mementoIndex];
        const { delay, command, props} = memento;
    
        if(null != command){
            command(_state);
        }else{
            for(let prop in props){
                _state[prop] = props[prop]
            }
        }
        return delay
    }

    // INTERFACE
    this.start = function(){
        _timeStamp = Date.now();
        _stateMemento.push({
            props: clone(state),
            delay: 5000
        })
    }

    this.play = function(){
        _isPlaying    = true;
        _mementoIndex = 0;
        doPlay()
    }

    this.pause = function(){
        _isPlaying = !_isPlaying
        if(_isPlaying){
            doPlay();
        }else{
            clearTimeout(_delayId);
        }
    }

    this.next = function(){
        _mementoIndex++
        changeState();
    }   

    this.prev = function(){
        _mementoIndex--
        changeState();
    }

    this.addProp = function(prop, value){
        if(_isPlaying){
            // Early return
            return
        }
        _stateMemento.push({
            delay: Date.now() - _timeStamp,
            props: {
                [prop]: clone(value)
            }
        })
        _timeStamp = Date.now();
    }
    
    this.addCommand = function(command){
        if(_isPlaying){
            // Early return
            return
        }
        _stateMemento.push({
            delay: Date.now() - _timeStamp,
            command
        })
        _timeStamp = Date.now();
    }
}

export { Memento }
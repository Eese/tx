import { callPOST, callGET, callPATCH, callPUT, callDELETE } from '../../../libs/data-utils/dataLoader.js'
import { APPWRITE_URL } from './URLs.js'
import { mapRootAPI } from './mapRoot.js'

async function appwritePOST(path, body, headers, errorHandler){
    const response = await callPOST(APPWRITE_URL+path, body, headers, errorHandler);
    return mapRootAPI(response)
}

async function appwriteGET(path, headers, errorHandler){
    const response = await callGET(APPWRITE_URL+path, headers, errorHandler);
    return mapRootAPI(response)
}

async function appwritePATCH(path, body, headers, errorHandler){
    const response = await callPATCH(APPWRITE_URL+path, body, headers, errorHandler);
    return mapRootAPI(response)
}

async function appwritePUT(path, body, headers, errorHandler){
    const response = await callPUT(APPWRITE_URL+path, body, headers, errorHandler);
    return mapRootAPI(response)
}

async function appwriteDELETE(path, body, headers, errorHandler){
    const response = await callDELETE(APPWRITE_URL+path, body, headers, errorHandler);
    return mapRootAPI(response)
}

export {
    appwritePOST,
    appwriteGET,
    appwritePATCH,
    appwritePUT,
    appwriteDELETE
}
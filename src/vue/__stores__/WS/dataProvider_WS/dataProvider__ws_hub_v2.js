/* WRAPPERS */
import { appwritePUT, appwriteGET, appwritePOST } from '../../commons/appwrite.js'

/* LIBS */
import { callGET }            from '../../../../libs/data-utils/dataLoader.js'
import { mapRoot }            from '../../commons/mapRoot.js'
import { formatFromDateString, 
         formatToDateString}  from '../../../../libs/DateUtils.js'
import { populateDatabase }   from './dataProvider__populateDatabase.js'

let is_api = false;

function parseWorkSchedule(workSchedule){
    workSchedule.forEach((areaData)=>{
        let propList   = Object.getOwnPropertyNames(areaData);
        let schemaList = ['areaId', 'areaName', 'timezoneList', 'areaTaskList', 'day_schedule', 'day_result'];
        propList.forEach((prop)=>{
            handleNotValidString(prop, schemaList, 'workSchedule');
        });

        const {
            areaId,
            areaName,
            timezoneList,
            areaTaskList,
            day_schedule,
            day_result
        } = areaData;

        handleEmptyString(areaId,   'areaId');
        handleEmptyString(areaName, 'areaName');
        handleNumber(day_schedule,  'day_schedule');
        handleNumber(day_result,    'day_result');
        handleArray(timezoneList,   'timezoneList');

        timezoneList.forEach((timezone)=>{
            const propList   = Object.getOwnPropertyNames(timezone);
            const schemaList = ['hour', 'hour_result', 'hour_schedule', 'taskList', 'tagList'];
            propList.forEach((prop)=>{
                handleNotValidString(prop, schemaList, 'timezone');
            });

            const {
                hour,
                hour_result,
                hour_schedule,
                taskList
            } = timezone;

            if(null != taskList){
                handleArray(taskList, 'taskList');
                taskList.forEach((task)=>{
                    handleDataStructure(task, 'task');
                    const propList   = Object.getOwnPropertyNames(task);
                    const schemaList = ['duration', 'taskName'];
                    propList.forEach((prop)=>{
                        handleNotValidString(prop, schemaList, 'task');
                    });
                    const {
                        taskName,
                        duration
                    }= task;
                    handleEmptyString(taskName, 'taskName');
                    handleNumber(duration, 'duration');
                });
            }

            handleArray(areaTaskList, 'areaTaskList');
            areaTaskList.forEach((taskName)=>{
                handleEmptyString(taskName, 'taskName');
            });

            handleNumber(hour,          'hour');
            handleNumber(hour_result,   'hour_result');
            handleNumber(hour_schedule, 'hour_schedule');
        });
    });
}

function parseAllAreasSchedule(allAreasSchedule){
    handleArray(allAreasSchedule, 'allAreasSchedule');
    let index=0;
    allAreasSchedule.forEach((areaData)=>{
        handleDataStructure(areaData, `allAreasSchedule[${index}]`);

        const propList     = Object.getOwnPropertyNames(areaData);
        const schemaList = ['totalAreasSchedule', 'shift', 'areaList', 'hour'];
        propList.forEach((prop)=>{
            handleNotValidString(prop, schemaList, `allAreasSchedule[${index}] unexpected property:${prop}`);
        });

        const {
            shift,
            totalAreasSchedule,
            areaList,
            hour
        } = areaData;

        handleNumber(hour,               'hour');
        handleNumber(shift,              'shift');
        handleArray(areaList,            'areaList');
        handleNumber(totalAreasSchedule, 'totalAreasSchedule');

        let areaDataIndex = 0;
        areaList.forEach((areaData)=>{
            handleDataStructure(areaData, `areaList[${areaDataIndex}]`);

            const propList     = Object.getOwnPropertyNames(areaData);
            const schemaList = ['areaId', 'areaName', 'hour_schedule'];
            propList.forEach((prop)=>{
                handleNotValidString(prop, schemaList, `areaList[${index}] unexpeted property: ${prop}`);
            });
            const {
                areaId,
                areaName,
                hour_schedule
            } = areaData;

            handleEmptyString(areaId,   'areaId');
            handleEmptyString(areaName, 'areaName');
            handleNumber(hour_schedule, 'hour_schedule');
        });
    });
}

async function getDateWSandReport(dateToLoad){
    handleEmptyString(dateToLoad, 'dateToLoad');
    const URL =  is_api ? 
                           `http://func-analytic-prot-jpeast-001-dev.azurewebsites.net/api/report`
                          :
                           `/json/new-ws-v2/${dateToLoad}.json`;
    /* CALL API */
    const headers = {
        "store_id": "3b2e9880-29ce-4850-aad3-550875d5b4c4", //temp holder for store
        "from_date": dateToLoad,
        "to_date": dateToLoad,
        "type": "workSchedule"
    };
    
    let response;
    /*
    response = await callGET(URL, headers);
    response = mapRoot(response);
    */
    
    response = await appwriteGET('report/'+dateToLoad);
    console.log(response)
    
    /*
    setTimeout(()=>{
      　console.log('CALL POPULATE!!!!!')
        populateDatabase(response);
    }, 10000);
    */

    handleDataStructure(response, 'response');
    
    const propList     = Object.getOwnPropertyNames(response);
    const schemaList = ['date', 'workSchedule', 'allAreasSchedule', 'wsName', 'wsId'];
    propList.forEach((prop)=>{
        handleNotValidString(prop, schemaList, 'response');
    });

    const { date,
            workSchedule,
            allAreasSchedule,

            wsId,
            wsName
        } = response;

    handleEmptyString(date,       'date');
    handleEmptyString(wsName,     'wsName');
    handleEmptyString(wsId,       'wsId');
    handleArray(workSchedule,     'workSchedule');
    handleArray(allAreasSchedule, 'allAreasSchedule');

    parseWorkSchedule(workSchedule);
    parseAllAreasSchedule(allAreasSchedule);

    const totalAreas = workSchedule.length;

    return {
        date,
        wsId,
        wsName,
        workSchedule,
        allAreasSchedule,
        totalAreas
    }
}

// CREATE
async function createWS_FAKE(parsedData){
    const ws = clone(parsedData);

    delete ws.totalAreas
    
    const {
          date
         ,workSchedule
    } = ws;

    ws.wsId   = date;
    ws.wsName = date;

    workSchedule.forEach((ws )=>{
        delete ws.day_result;
        const {timezoneList} = ws;
        timezoneList.forEach((timezoneData)=>{
            delete timezoneData.hour_result
            delete timezoneData.tagList
        });
    });
}

// READ
async function ws_hub_API(startDateString, endDateString, isApi){
    is_api             = isApi;
    const startDate    = new Date(formatFromDateString(startDateString));
    const endDate      = new Date(formatFromDateString(endDateString));
    const initialDate  = startDate < endDate ? startDate : endDate;
    const finalDate    = startDate > endDate ? startDate : endDate;

    const wsDateList = [];

    let totalAreas     = 0;

    for (let currentDate = new Date(initialDate); currentDate <= finalDate; currentDate.setDate(currentDate.getDate() + 1)) {
        const dateToload = formatToDateString(currentDate.getTime());
        const parsedData = await getDateWSandReport(dateToload);
        
        totalAreas       = Math.max(parsedData.totalAreas, totalAreas);
        wsDateList.push(parsedData);
    }

    return {
        wsDateList,
        totalAreas
    }
}

// UPDATE
async function ws_updateProxyTask(proxy__wsDateList, dayIndex, areaId, hourIndex, taskIndex, newDuration){
    handleArray(proxy__wsDateList, 'proxy__wsDateList');
    handleNumber(dayIndex,         'dayIndex');
    const rawData = proxy__wsDateList[dayIndex];

    handleDataStructure(rawData, 'rawData');
    handleEmptyString(areaId,    'areaId');
    handleNumber(hourIndex,      'hourIndex');
    handleNumber(taskIndex,      'taskIndex');
    handleNumber(newDuration,    'newDuration');

    const {workSchedule, allAreasSchedule} = rawData;
    const areaData = workSchedule.find((area)=>{
        return area.areaId == areaId
    });

    if(null == areaData){
        // Early return
        return
    }
    const { timezoneList }   = areaData;
    const timezone           = timezoneList[hourIndex];
    const { taskList, hour } = timezone;
    const task               = taskList[taskIndex];
    const oldDuration        = task.duration;

    areaData.day_schedule   -= oldDuration;
    areaData.day_schedule   += newDuration;

    timezone.hour_schedule  -= oldDuration;
    timezone.hour_schedule  += newDuration;
    
    task.duration            = newDuration;

    const allAreasTimezone                     = allAreasSchedule[hourIndex];
          allAreasTimezone.totalAreasSchedule -= oldDuration;
          allAreasTimezone.totalAreasSchedule += newDuration;
    const {areaList}                           = allAreasTimezone;

    areaList.forEach((area)=>{
        if(area.areaId == areaId){
            area.hour_schedule = timezone.hour_schedule;
        }
    });
    
    return proxy__wsDateList
}

async function ws_put_task(ws, StoreId) {
    handleDataStructure(ws, 'ws');

    const {
        wsId:id,
        wsName:Name,
        workSchedule
    } = ws;
    
    handleEmptyString(id,      'id');
    handleEmptyString(Name,    'Name');
    handleEmptyString(StoreId, 'StoreId');
    handleArray(workSchedule,  'workSchedule');

    const WorkSchedule = [];

    workSchedule.forEach((area)=>{
        const {
            areaId:AreaId,
            timezoneList
        } = area;

        const TimezoneList = {};
        timezoneList.forEach((timezone, hourIndex)=>{
            const {
                duration
            } = timezone;
            /*
            TimezoneList[hourIndex] = {

            }
            */
        });

    });
}

async function ws_put_shift(ws, StoreId) {
    handleDataStructure(ws, 'ws');
}

async function ws_addTask(proxy__wsDateList, dayIndex, areaId, hourIndex, taskName, durationInput, forceAddition){
    handleArray(proxy__wsDateList, 'proxy__wsDateList');
    handleNumber(dayIndex,         'dayIndex');
    const rawData = proxy__wsDateList[dayIndex];
    
    const {workSchedule, 
           allAreasSchedule} = rawData;
    handleEmptyString(areaId, 'areaId');
    const areaData = workSchedule.find((area)=>{
        return area.areaId == areaId
    });

    if(null == areaData){
        // Early return
        return
    }

    handleEmptyString(taskName,   'taskName');

    const { timezoneList, 
        areaTaskList } = areaData;
    const timezone     = timezoneList[hourIndex];
    const { taskList,
        hour }         = timezone;

    let indexToAdd = null;
    const taskToAdd = taskList.find((task, index)=>{
        if(task.taskName == taskName){
            indexToAdd = index;
            return true
        }
    });

    if(null != taskToAdd && !forceAddition){
        
        // Early return
        return
    }

    const duration = durationInput || 5;
    if(null != indexToAdd && null != taskList[indexToAdd]){
        taskList[indexToAdd].duration += duration;
    }else{
        console.log('PUSH!!!!')
        taskList.push({
            taskName,
            duration
        });
    }

    let hour_schedule = 0;
    taskList.forEach((task)=>{
        hour_schedule += task.duration;
    });

    timezone.hour_schedule = hour_schedule;

    let day_schedule = 0
    timezoneList.forEach(({hour_schedule})=>{
        day_schedule += hour_schedule || 0;
    });

    allAreasSchedule[hourIndex].totalAreasSchedule += duration;

    const {areaList} = allAreasSchedule[hourIndex];

    const areaForAll = areaList.find((area)=>{
        return area.areaId == areaId
    });

    areaForAll.hour_schedule = hour_schedule;
    return proxy__wsDateList
}

async function ws_moveTask(proxy__wsDateList, dayIndex, areaId, fromHourIndex, toHourIndex, taskName){
    handleArray(proxy__wsDateList, 'proxy__wsDateList');
    handleNumber(dayIndex,         'dayIndex');
    handleEmptyString(areaId,      'areaId');
    handleNumber(fromHourIndex,    'fromHourIndex');
    handleNumber(toHourIndex,      'toHourIndex');
    handleEmptyString(taskName,    'taskName');

    const rawData            = proxy__wsDateList[dayIndex];
    const {workSchedule, 
           allAreasSchedule} = rawData;
    const areaData = workSchedule.find((area)=>{
        return area.areaId == areaId
    });

    if(null == areaData){
        // Early return
        return
    }

    handleEmptyString(taskName,   'taskName');

    const { timezoneList, 
        areaTaskList } = areaData;
    const timezone     = timezoneList[fromHourIndex];
    const { taskList,
        hour }         = timezone;

    const movingTask = taskList.find((task)=>{
        return task.taskName == taskName
    });

    if(null == movingTask){
        // Early return
        return
    }

    await ws_removeTask(proxy__wsDateList, dayIndex, areaId, fromHourIndex, taskName);

    const forceAddition = true;
    await ws_addTask(proxy__wsDateList, dayIndex, areaId, toHourIndex, taskName, movingTask.duration, forceAddition);
    return proxy__wsDateList
}

// DELETE
async function ws_removeTask(proxy__wsDateList, dayIndex, areaId, hourIndex, taskName){
    handleArray(proxy__wsDateList, 'proxy__wsDateList');
    handleNumber(dayIndex,         'dayIndex');
    const rawData = proxy__wsDateList[dayIndex];

    handleDataStructure(rawData, 'rawData');
    handleEmptyString(areaId,    'areaId');
    handleNumber(hourIndex,      'hourIndex');
    handleEmptyString(taskName,  'taskName');

    const {workSchedule, allAreasSchedule} = rawData;
    const areaData = workSchedule.find((area)=>{
        return area.areaId == areaId
    });

    if(null == areaData){
        // Early return
        return
    }
    const { timezoneList }  = areaData;
    const timezone          = timezoneList[hourIndex];
    const { taskList }      = timezone;

    let indexToRemove = null;
    const task = taskList.find((taskSearch, index)=>{
        if(taskSearch.taskName == taskName){
            // Early return
            indexToRemove = index;
            return true
        }
        return false
    });

    if(null == task){
        // Early return
        return
    }
    
    const oldDuration       = task.duration;
    areaData.day_schedule  -= oldDuration;
    timezone.hour_schedule -= oldDuration;

    const allAreasTimezone               = allAreasSchedule[hourIndex];
    allAreasTimezone.totalAreasSchedule -= oldDuration;

    const {areaList} = allAreasTimezone;

    areaList.forEach((area)=>{
        if(area.areaId == areaId){
            area.hour_schedule = timezone.hour_schedule;
        }
    });

    taskList.splice(indexToRemove, 1);
    return proxy__wsDateList
}

export {
    ws_hub_API,
    createWS_FAKE,
    ws_updateProxyTask,
    ws_put_task,
    ws_put_shift,
    ws_addTask,
    ws_moveTask,
    ws_removeTask
}
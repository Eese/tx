/* WRAPPERS */
import { appwritePUT, appwriteGET, appwritePOST } from '../../commons/appwrite.js'


async function initializeDataLoading(wsDateList){
    return
    const {date}   = wsDateList[0];

    const response = await appwriteGET('report/'+date);
    console.log(response)
    /*
    const {workSchedule} = wsJson;
    const areaDict = {}

    workSchedule.forEach(area => {
        const {areaId} = area;
        let areaData   = areaDict[areaId];
        if(null == areaData){
            areaDict[areaId] = area;
            areaData = areaDict[areaId];
        }
        areaData.day_result  = 0;
        const {timezoneList} = areaData;
        timezoneList.forEach((timezone)=>{
            timezone.hour_result = 0;
        });
    });

    resultJson.forEach((result)=>{
        const {areaId, areaName, timezoneList} = result;
        const areaData = areaDict[areaId];
        timezoneList.forEach((timezone, hourIndex)=>{
            const areaTimezone    = areaData.timezoneList[hourIndex];
            const {hour, tagList} = timezone;
            tagList.forEach(({result})=>{
                areaData.day_result       = areaData.day_result || 0;
                areaData.day_result      += result;
                areaTimezone.hour_result += result;
            });
            areaTimezone.tagList = tagList;
        });
    });

    console.log(workSchedule)
    */
}

export {
    initializeDataLoading
}
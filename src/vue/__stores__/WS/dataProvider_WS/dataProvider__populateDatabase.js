/* WRAPPERS */
import { callGET, callPOST } from '../../../../libs/data-utils/dataLoader.js'
import { mapRootAPI, mapRoot } from '../../commons/mapRoot.js';
import { appwritePUT, appwriteGET, appwritePOST } from '../../commons/appwrite.js'

import { APPWRITE_URL, COMPARISON_API } from '../../commons/URLs.js'

function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

const STORE_ID = '605564d5-4723-4b95-871c-71c72e0ca455';

async function createWS(wsDateInput) {
    return
    const wsDate = clone(wsDateInput);
    const {
        date,
        workSchedule
    } = wsDate

    
    if('2024-02-05' != date){
        // Early return
        return
    }

    delete wsDate.wsId;
    delete wsDate.date;
    
    workSchedule.forEach(areaData=>{
        delete areaData.day_result;

        const {
            timezoneList
        } = areaData;

        timezoneList.forEach(timezone =>{
            delete timezone.hour_result;
            delete timezone.hour_onSchedule;
            delete timezone.hour_overwork;
            delete timezone.hour_underwork;
            delete timezone.tagList;
        });
    });

    const dayNames = ['月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日', '日曜日'];
    
    for(let index=0; index < dayNames.length; index++){
        const wsName          = dayNames[index];
        const jsonData        = clone(wsDate);
              jsonData.wsName = wsName;
        const wsResponse = await appwritePOST('ws_json', {fileName:wsName, jsonData});
        console.log(wsResponse)
    }
    
    const jsonData = clone(wsDate);
    jsonData.wsName = 'TEMPLATE';
    const wsResponse = await appwritePOST('ws_json', {fileName:jsonData.wsName, jsonData});
    
    console.log('FINISHED TO POPULATE WS')
};

async function createResult(wsDateList) {
    return
    const {date:fileName, workSchedule:jsonData} = clone(wsDateList);
    const areaList = [];

    jsonData.forEach((rawArea)=>{
        const {
            areaId,
            areaName,
            day_result,
            timezoneList:inflatedList
        } = rawArea;

        const area = {
            areaId,
            areaName,
        }
        areaList.push(area)
        
        const timezoneList = [];
        inflatedList.forEach((timezone)=>{
            const {hour, hour_result} = timezone;
            
            if(0 < hour_result){
                const tagList = [{
                    result: hour_result,
                    tagId: 'NONE'
                }];
                timezoneList.push({
                    hour,
                    tagList
                })
            }
        });
        area.timezoneList = timezoneList;
    });
    
    let wsResponse = await appwritePOST('ws_result', {fileName, jsonData:areaList});
}

const populateDatabase = ()=>{};

export {
    populateDatabase
}
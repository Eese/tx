async function read_itemsPerPage(){
    let itemsPerPage = localStorage.getItem("itemsPerPage");
    if(null == itemsPerPage){
        // Early return
        return
    }
    itemsPerPage = Number(itemsPerPage);
    handleNumber(itemsPerPage, 'itemsPerPage');
    return itemsPerPage
}

async function update_itemsPerPage(itemsPerPage){
    handleNumber(itemsPerPage, 'itemsPerPage');
    localStorage.setItem("itemsPerPage", itemsPerPage);
}


export {
    read_itemsPerPage,
    update_itemsPerPage
 }
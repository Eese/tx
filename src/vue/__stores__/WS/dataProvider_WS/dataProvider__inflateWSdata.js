import { getDatesetFromString } from '../../../../libs/DateUtils.js'

function inflate_shiftData(wsDateList){
    const shiftDateList = [];
    let shiftPick  = 0;
    
    wsDateList.forEach(({date, allAreasSchedule, wsName}, dayIndex)=>{
        let day_shift          = 0;
        let day_total_schedule = 0;
        let day_understaffing  = 0; 
        let day_overstaffing   = 0;
        let day_shiftNeeded    = 0;
        let day_netDiff        = 0;
        let shift_timezoneList = [];

        allAreasSchedule.forEach((shiftData)=>{
            const {
                hour,
                shift,
                totalAreasSchedule
            } = shiftData;
            
            // HOUR
            let   hour_shiftNeeded   = totalAreasSchedule/60;
            const hour_shift         = shift;
            const hour_shift_diff    = roundDecimals(hour_shift-hour_shiftNeeded,1);
            const hour_understaffing = hour_shift_diff < 0 ? Math.abs(hour_shift_diff) : 0;
            const hour_overstaffing  = hour_shift_diff > 0 ? Math.abs(hour_shift_diff) : 0;
                  hour_shiftNeeded   = roundDecimals(hour_shiftNeeded, 1);

            shift_timezoneList.push({
                hour_shift,
                hour_shift_diff,
                hour_understaffing,
                hour_overstaffing,
                hour_shiftNeeded,
                totalAreasSchedule
            })

            // DAY
            day_shift          += hour_shift;
            day_total_schedule += totalAreasSchedule;
            day_understaffing  += hour_understaffing;
            day_overstaffing   += hour_overstaffing;
            day_shiftNeeded    += hour_shiftNeeded;
            day_netDiff        += (hour_shift - hour_shiftNeeded);

            shiftPick = Math.max(day_understaffing, day_overstaffing, day_shift, shiftPick);
        });

        const dateSet   = getDatesetFromString(date);
        const dayName   = dateSet.dayName;
        const isWeekDay = dateSet.isWeekDay;

        const day_understaffing_percent = roundDecimals((day_understaffing/shiftPick)*100,1)      + '%';
        const day_overstaffing_percent  = roundDecimals((day_overstaffing/shiftPick)*100,1)       + '%';
        const day_net_percent           = roundDecimals((Math.abs(day_netDiff)/shiftPick)*100, 1) + '%';
        
        shiftDateList.push({
             wsName
            ,dayName
            ,date:dateSet.date
            ,isWeekDay
            ,day_shift
            ,day_understaffing
            ,day_overstaffing
            ,day_total_schedule
            ,day_shiftNeeded
            ,day_netDiff:roundDecimals(day_netDiff, 1)
            ,shift_timezoneList

            ,day_understaffing_percent
            ,day_overstaffing_percent
            ,day_net_percent
            ,shiftPick
        });
    });
   return shiftDateList
}

function inflateWSdata(wsDateList){
    const areaDict         = {};
    const areaList         = [];
    const allTimezoneDict  = {};
    const areaTimezoneDict = {};
    const averageDict      = {};
    const pickAreaDict     = {};
    let netPick            = 0;
    let pickHour           = 0;
    let dayPick            = 0;
    const totalDays        = wsDateList.length;
    
    wsDateList.forEach((wsDateData, dayIndex)=>{
        const {date:dateString, workSchedule, allAreasSchedule} = wsDateData;
        const timezoneAreasList = [];
        const timezoneAreas     = [];
        for(let hourIndex = 0; hourIndex < 24; hourIndex++){
            timezoneAreas.push([])
        }
        
        workSchedule.forEach((area, areaIndex)=>{
            const { areaId,
                    areaName,
                    day_schedule,
                    day_result,
                    areaTaskList,
                    timezoneList:timezoneRawList
            }  = area;

            let areaData = areaDict[areaId];
            if (null == areaData) {
                areaData = {
                    area,
                    dayData:{
                        areaId,
                        areaName,
                        areaTaskList,
                        dayList:[]
                    },
                    maxPick:0,
                    areasAndShiftList: allAreasSchedule
                };
                areaDict[areaId] = areaData;
                areaList.push(areaData);
            }

            const timezoneData = {
                areaId,
                areaName,
                hourPick:0,
                timezoneList:[]
            }
            
            const { dayData }  = areaData;
            
            let day_overwork   = 0;
            let day_onSchedule = 0;
            let day_underwork  = 0;

            const {timezoneList} = timezoneData;
            let hourPick         = 0;

            // GENERATE DATA
            timezoneRawList.forEach((timezone, hourIndex)=>{
                const {
                    hour,
                    hour_result,
                    hour_schedule,
                    taskList,
                } = timezone;

                let hour_underwork = 0;
                let hour_onSchedule = 0;
                let hour_overwork   = 0;

                let averageData = averageDict[areaId + '_' + hour];
                if(null == averageData){
                    averageData = {
                        hour_result_average     : 0   
                       ,hour_schedule_average   : 0
                       ,hour_overwork_average   : 0
                       ,hour_onSchedule_average : 0
                       ,hour_underwork_average  : 0
                       ,hourPick_average        : 0
                       ,percenOverworktList     : []
                   }
                   averageDict[areaId + '_' + hour] = averageData;
                }

                // OFF SCHEDULE - OVERWORKED
                if(0 == hour_schedule){
                    hourPick      = Math.max(hour_result, hourPick);
                    // HOUR
                    hour_overwork = hour_result
                    // DAY
                    day_overwork += hour_result;
                }else{
                    // ON SCHEDULE  - UNDERWORKED
                    if(hour_schedule > hour_result){
                        const diff      = hour_schedule - hour_result;
                        hourPick        = Math.max(hour_schedule, hourPick);
                        // HOUR
                        hour_underwork= diff; 
                        hour_onSchedule = hour_result;
                        // DAY
                        day_underwork  += diff;
                        day_onSchedule += hour_result;
                    }

                    // ON SCHEDULE  - OVERWORKED
                    if(hour_schedule < hour_result){
                        const diff      = hour_result - hour_schedule;
                        hourPick        = Math.max(hour_schedule, hourPick);
                        // HOUR
                        hour_overwork   = diff;
                        hour_onSchedule = hour_schedule;
                        // DAY
                        day_overwork   += diff;
                        day_onSchedule += hour_schedule;
                    }

                    // ON SCHEDULE  - 100%
                    if(hour_schedule == hour_result){
                        hour_onSchedule  = hour_result;
                        day_onSchedule  += hour_result;
                    }
                    
                    timezone.hour_underwork = hour_underwork;
                    timezone.hour_onSchedule = hour_onSchedule;
                    timezone.hour_overwork   = hour_overwork;
                    timezone.taskList        = taskList;
                }

                const areaTimezone = {
                    areaName,
                    areaId,
                    hour,

                    hourString:      hour < 10 ? '0' + hour: hour,
                    hour_result:     hour_result     || 0,
                    hour_schedule:   hour_schedule   || 0,
                    hour_overwork:   hour_overwork   || 0,
                    hour_onSchedule: hour_onSchedule || 0,
                    hour_underwork: hour_underwork || 0,
                    areaTaskList:    areaTaskList    || [],
                    taskList:        taskList        || []
                }

                areaTimezoneDict[areaId + '_' + dayIndex + '_' + hour] = areaTimezone;
                pickHour = Math.max(Math.abs(hour_overwork), Math.abs(hour_underwork));
                timezoneList.push(areaTimezone);
                timezoneAreas[hour].push(areaTimezone);

                if(null == timezoneAreasList[hour] ){
                    timezoneAreasList[hour] = [];
                }
                timezoneAreasList[hour].push(areaTimezone);

                if(null == pickAreaDict[areaId]){
                    pickAreaDict[areaId] = 0;
                }
                pickAreaDict[areaId] = Math.max(hourPick, pickAreaDict[areaId]);
            });
            
            timezoneData.hourPick = hourPick;
            const dateSet         = getDatesetFromString(dateString);
            const dayName         = dateSet.dayName;
            const date            = dateSet.date;
            const isWeekDay       = dateSet.isWeekDay;
            const { dayList }     = dayData;
            const day_netDiff     = day_schedule - day_result;

            dayList.push({
                // AREA
                areaId,
                areaName,
                // DAY
                dateString,
                date: date.length < 10 ? '0' + date : date,
                dayName: dayName,
                isWeekDay,
                // RAW METRICS
                day_result,
                day_schedule,
                // GRAPH METRICS
                day_overwork,
                day_onSchedule,
                day_underwork,
                day_netDiff,
                // TIMEZONE LIST
                timezoneData
            });

            dayPick          = Math.max(day_overwork, day_underwork, dayPick);
            const maxPick    = areaData.maxPick;
            areaData.maxPick = Math.max(day_overwork+day_underwork, maxPick);
            netPick          = Math.max(Math.abs(day_netDiff), netPick);
        });

        wsDateData.timezoneAreas    = timezoneAreas; // Each time zone has a list of areas.
        allTimezoneDict[dateString] = timezoneAreasList;
    });

    areaList.sort((a, b)=>{
        return Number(b.maxPick - a.maxPick)
    });

    areaList.forEach((areaData, index)=>{
        const {dayList} = areaData.dayData;
        const {areaId, areaName}  = areaData.area;

        dayList.forEach((daydata, dayIndex)=>{
            const {
                day_netDiff,
                day_overwork,
                day_underwork,
                timezoneData
            }= daydata;

            const day_underwork_percent   = roundDecimals((day_underwork/dayPick)*100,2) + '%';
            const day_overwork_percent    = roundDecimals((day_overwork/dayPick)*100,2)  + '%';
            const day_net_percent         = roundDecimals((Math.abs(day_netDiff)/netPick)*100, 2) + '%';
            daydata.day_underwork_percent = day_underwork_percent;
            daydata.day_overwork_percent  = day_overwork_percent;
            daydata.day_net_percent       = day_net_percent;
            
            const {
                timezoneList
            } = timezoneData;

            timezoneList.forEach((timezone, hourIndex)=>{
                const {
                    hour_overwork,
                    hour_underwork,
                    hour_result,

                } = timezone;
                const hourPick                 = pickAreaDict[areaId];
                const hour_underwork_percent   = roundDecimals((hour_underwork/hourPick)*100,2) + '%';
                const hour_overwork_percent    = roundDecimals((hour_overwork/hourPick)*100,2)  + '%';
                timezone.hour_underwork_percent  = hour_underwork_percent;
                timezone.hour_overwork_percent = hour_overwork_percent;

                const averageData = averageDict[areaId+'_'+hourIndex];
                if(null != averageData){
                    if(null == averageData.percenOverworktList){
                        averageData.percenOverworktList = [];
                        console.error(`averageData.percenOverworktList is undefined, in the dayIndex: ${dayIndex}, The area ${areaName},  areaId: ${areaId} with hour: ${hourIndex} is undefined.`)
                    }
                    averageData.percenOverworktList.push({hour_overwork_percent, hour_overwork});
                    averageData.hour_result_average    += hour_result;
                    averageData.hour_overwork_average  += hour_overwork;
                    averageData.hour_underwork_average += hour_underwork;
                    averageData.hourPick_average        = hourPick;
                }else{
                    console.error(`averageData is undefined, in the dayIndex: ${dayIndex}, The area ${areaName},  areaId: ${areaId} with hour: ${hourIndex} is undefined.`)
                }
            });

        });
    });

    for(let prop in averageDict){
        const averageData = averageDict[prop];
        const areaId      = prop.split('_')[0];
        const hour        = Number(prop.split('_')[1]);
        const hourPick    = pickAreaDict[areaId];

        averageData.hour_result_average    /= totalDays;
        averageData.hour_schedule_average  /= totalDays;
        averageData.hour_overwork_average  /= totalDays;
        averageData.hour_underwork_average /= totalDays;

        let {
            hour_result_average
           ,hour_underwork_average
           ,hour_overwork_average
        } = averageData;

        hour_result_average    = roundDecimals(hour_result_average, 1);
        hour_overwork_average  = roundDecimals(hour_overwork_average, 1);
        hour_underwork_average = roundDecimals(hour_underwork_average, 1);

        averageData.hour_result_average    = hour_result_average;
        averageData.hour_overwork_average  = hour_overwork_average;
        averageData.hour_underwork_average = hour_underwork_average;

        averageData.hour_overwork_average_percent  = roundDecimals((hour_overwork_average  / hourPick ) * 100 ,2) + '%';
        averageData.hour_underwork_average_percent = roundDecimals((hour_underwork_average / hourPick ) * 100 ,2) + '%';
    }

    const shiftDataList = inflate_shiftData(wsDateList);

    return {
        areaDict,
        areaList,
        shiftDataList,
        allTimezoneDict,
        areaTimezoneDict,
        averageDict
    }
}

export { inflateWSdata }
/* WRAPPERS */
import { callGET, callPOST } from '../../../../libs/data-utils/dataLoader.js'
import { mapRootAPI, mapRoot } from '../../commons/mapRoot.js';
import { appwritePUT, appwriteGET, appwritePOST } from '../../commons/appwrite.js'

async function updateWS(wsInput){
    const wsDate = clone(wsInput);
    const {wsId:fileId, wsName:fileName, workSchedule} = wsDate;

    console.log('updateWS fileId', fileId);
    
    delete wsDate.wsId;
    delete wsDate.date;
    delete wsDate.wsDate;
    delete wsDate.totalAreas;
    delete wsDate.timezoneAreas;
    
    workSchedule.forEach(areaData=>{
        delete areaData.day_result;

        const {
            timezoneList
        } = areaData;

        timezoneList.forEach(timezone =>{
            delete timezone.hour_result;
            delete timezone.hour_onSchedule;
            delete timezone.hour_overwork;
            delete timezone.hour_underwork;
            delete timezone.tagList;
        });
    });

    console.log('fileId', fileId);
    
    const response = await appwritePUT('ws_json', {
        jsonData:wsDate,
        fileName,
        fileId
    });
    console.log('response', response);
    return response
}

export { updateWS }
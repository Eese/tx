/* VUE */
import { reactive, computed, readonly } from 'vue'

/* DATA PROVIDER */
import { update_itemsPerPage,
         read_itemsPerPage } from './dataProvider_WS/dataProvider__settings.js'

import { initializeDataLoading } from './dataProvider_WS/dataProvider__ws_builder.js'

/* WRAPPER */
import { updateWS } from './dataProvider_WS/dataProvider__updateWS.js'

const FAKE_STORE_ID = "605564d5-4723-4b95-871c-71c72e0ca455";

import { 
    ws_hub_API,
    ws_removeTask,
    ws_put_shift,
    ws_put_task,
    ws_updateProxyTask,
    ws_addTask,
    ws_moveTask
} from './dataProvider_WS/dataProvider__ws_hub_v2.js'

import { inflateWSdata } from './dataProvider_WS/dataProvider__inflateWSdata.js'

/* HELPERS */
import { getDateSetByString, 
         getDateSetByTime } from './helpers/helpers.js'

/* MEMENTO */
import { Memento } from '../commons/Memento.js'
import { handleDataStructure } from '../../../libs/errorHandling.js'
import { dayNameList } from '../../../libs/DateUtils.js'

const DEFAULT_ITEMS_PER_PAGE = 3;

let proxy__wsDateList        = null;
let proxy__areaTimezoneDict  = null;

let timeoutId = 0;
let isUpdating = false;
const commandDict = {};

function mutate(prop, value, command){
    if(null == command){
        state[prop] = value;
        memento.addProp(prop, value);
    }else{
        command(state);
        memento.addCommand(command);
    }
}

function getHomePath(params={}){
    handleDataStructure(params, 'params');

    const {from, to, dayIndex} = params;
    handleIfNotNull(handleEmptyString, from,     'from');
    handleIfNotNull(handleEmptyString, to,       'to');
    handleIfNotNull(handleNumber,      dayIndex, 'dayIndex');

    let fromFinal = from;
    let toFinal   = to;

    if(null == fromFinal || null == to){
        fromFinal = state.startDateSet.dateString;
        toFinal   = state.endDateSet.dateString;
    }

    let dayIndexFinal = 0;
    if(null != dayIndex){
        dayIndexFinal = dayIndex;
    }else if(null != state.dayIndex){
        dayIndexFinal = state.dayIndex;
    }

    const issueIndex = state.issueIndex || 0;
    const hourIndex  = state.hourIndex  || 0;
    
    const path = `/home/from/${fromFinal}/to/${toFinal}/day/${dayIndexFinal}/hour/${hourIndex}/issue/${issueIndex}`;

    return path
}

function getAreaPath(params){
    const {areaId, from, to, dayIndex, hourIndex} = params;
    handleDataStructure(params, 'params');

    handleIfNotNull(handleEmptyString, areaId,    'areaId');
    handleIfNotNull(handleEmptyString, from,      'from');
    handleIfNotNull(handleEmptyString, to,        'to');
    handleIfNotNull(handleNumber,      dayIndex,  'dayIndex');
    handleIfNotNull(handleNumber,      hourIndex, 'hourIndex');

    const finalAreaId = null == areaId ? state.currentAreaId : areaId;

    let finalFrom = from;
    let finalTo   = to;
    
    if(null == finalFrom || null == finalTo){
        finalFrom = state.startDateSet.dateString;
        finalTo   = state.endDateSet.dateString;
    }

    let finalDayIndex = 0;
    if(null != dayIndex){
        finalDayIndex = dayIndex;
    }else if(null != state.dayIndex){
        finalDayIndex = state.dayIndex;
    }

    let finalHourIndex = 0;
    if(null != hourIndex){
        finalHourIndex = hourIndex;
    }else if(null != state.hourIndex){
        finalHourIndex = state.hourIndex;
    }

    const issueIndex = state.issueIndex || 0;
    const isAverage  = state.isAverage ? 1 : 0;
    const path       = `/area/${finalAreaId}/from/${finalFrom}/to/${finalTo}/day/${finalDayIndex}/hour/${finalHourIndex}/issue/${issueIndex}/average/${isAverage}`;
    return path
}

async function setCoreState(wsDateList, isUpdate){
    initializeDataLoading(wsDateList);
    const inflatedData = inflateWSdata(wsDateList);
    mutate('inflatedData', inflatedData);
    state.canSave = Boolean(isUpdate);
    console.log('canSave', state.canSave);
    console.log('isUpdating', state.isUpdating);
}

/* STATE */
const state = reactive({
     areaDateString   : null
    ,areaReportList   : null

    ,canSave          : false
    ,currentAreaId    : null
    ,currentAreaData  : null

    ,dateWithDataList : null
    ,dateList         : null
    ,dayStringList    : null
    ,dayNameList      : null
    ,dayIndex         : null

    ,startDateSet     : null
    ,endDateSet       : null
    ,endTime          : null

    ,hourList         : null
    ,hourIndex        : null
    ,hourIndexHover   : null

    ,inflatedData     : null
    ,isAverage        : false
    ,issueIndex       : 0
    ,isUpdating       : false
    ,itemsPerPage     : DEFAULT_ITEMS_PER_PAGE

    ,params           : null
    ,pathName         : null
    ,newPath          : null

    ,startTime        : null

    ,totalAreas       : 0

});

const memento = new Memento(state);

/* MUTATE */
const mutations = {
    // INITIAL QUERYES
    async reactToParams(pathName, params, query){
        mutate('pathName', pathName);
        mutate('params', params);

        if('home' == pathName || 'area' == pathName){
            let {
                areaId,
                from,
                to,
                dayIndex,
                hourIndex,
                issueIndex,
                isAverage
            } = params;

            const {
                is_api
            } = query;
            mutate('is_api', is_api == '1' ? true : false);
            
            mutate('isAverage', isAverage == '1' ? true : false);
            
            handleEmptyString(from, 'from');
            handleEmptyString(to, 'to');
    
            dayIndex  = Number(dayIndex);
            hourIndex = Number(hourIndex);
    
            handleNumber(dayIndex,  'dayIndex');
            handleNumber(hourIndex, 'hourIndex');
    
            // API CALL
            await actions.read_RangeData(from, to, dayIndex, state.is_api);
    
            issueIndex = Number(issueIndex);
            handleNumber(issueIndex, 'issueIndex');    

            mutate('issueIndex', issueIndex);         
            mutate('dayIndex',   dayIndex);
            mutate('hourIndex',  Math.max(Math.min(hourIndex, 23), 0));

            if('area' == pathName){
                handleEmptyString(areaId, 'areaId');
                // API CALL
                await actions.read_areaDayById(areaId, dayIndex);
            }
        }

        mutate('pathName', pathName);
    }

    // Routing
    ,async back_home(){
        const path = getHomePath();
        mutate('newPath', path);
    }
    ,async go_to_HomeRange(startDateString, endDateString){
        const path = getHomePath({startDateString, endDateString});
        mutate('issueIndex', 0);
        mutate('dayIndex',   0);
        mutate('hourIndex',  0);
        mutate('newPath',    path);
    }
    ,async go_to_area(areaId){
        handleEmptyString(areaId, 'areaId');
        const path = getAreaPath({areaId});
        mutate('newPath', path);
    }

    // Indexes
    ,async setHourIndex(hourIndex){
        handleIfNotNull, (handleNumber,hourIndex, 'hourIndex');
        const path = getAreaPath({hourIndex});
        mutate('newPath', path);
    }
    ,async sethHourIndexHover(hourIndexHover){
        if(null == hourIndexHover){
            // Early return
            mutate('hourIndexHover', null);
            return
        }
        handleNumber(hourIndexHover, 'hourIndexHover');
        mutate('hourIndexHover', hourIndexHover);
    }
    ,async setDayIndex(dayIndex){
        handleNumber(dayIndex, 'dayIndex');
        let path;
        switch(state.pathName){
            case 'home':
                path = getHomePath({dayIndex});
            break
            case 'area':
                path = getAreaPath({dayIndex});
            break
        }
        mutate('newPath', path);
    }
    ,async switchView(){
        mutate('isAverage', !state.isAverage);
        mutations.go_to_area(state.currentAreaId);
    }
    ,async setIssueIndex(issueIndex){
        handleNumber(issueIndex, 'issueIndex');
        mutate('issueIndex', issueIndex);  
    }
}

/* ACTIONS */
const actions ={
    // READ
     async read_RangeData(startTime, endTime, dayIndex){
        handleEmptyString(startTime, 'startTime');
        handleEmptyString(endTime,   'endTime');
        handleNumber(dayIndex,       'dayIndex');
        
        mutate('dayIndex',  dayIndex);
        
        if(startTime == state.startTime && endTime == state.endTime && null != proxy__wsDateList){
            // Early return
            return
        }

        mutate('startTime', startTime);
        mutate('endTime',   endTime);
        // API CALL
        const result = await ws_hub_API(startTime, endTime, state.is_api);
        
        const {
            wsDateList,
            totalAreas
        } = result;

        proxy__wsDateList  = clone(wsDateList);
        const itemsPerPage = await read_itemsPerPage();
        if(null == itemsPerPage){
            actions.update_itemsPerPage(Math.min(totalAreas, DEFAULT_ITEMS_PER_PAGE));
        }else{
            mutate('itemsPerPage', itemsPerPage);
        }
        
        const startDateSet   = getDateSetByString(startTime);
        const endDateSet     = getDateSetByString(endTime);
        const initialDate    = startDateSet.time;
        const finalDate      = endDateSet.time;
        const dateList       = [];
        const dateStringList = [];
        const dayNameList    = [];
        for (let currentDate = new Date(initialDate); currentDate <= finalDate; currentDate.setDate(currentDate.getDate() + 1)) {
            const { dayName, dateSlashed, dateString} = getDateSetByTime(currentDate.getTime());
            dateList.push(localize(dayName) + ' ' +  dateSlashed);
            dateStringList.push(dateString);
            dayNameList.push(dayName);
        }

        const dateString = dateStringList[dayIndex];
        handleEmptyString(dateString, 'dateString');

        mutate('totalAreas'    , totalAreas);
        mutate('startDateSet'  , startDateSet);
        mutate('endDateSet'    , endDateSet);
        mutate('dateList'      , dateList);
        mutate('dateStringList', dateStringList);
        mutate('dayNameList'   , dayNameList);
        mutate('areaDateString', null);
        
        await setCoreState(wsDateList);
    }
    ,async read_areaDayById(areaId, dayIndex){
        if(null == state.inflatedData){
            // Early return
            return
        }
        handleEmptyString(areaId, 'areaId');
        handleNumber(dayIndex,    'dayIndex');

        mutate('currentAreaId', areaId);
        mutate('dayIndex',      dayIndex);

        const dateString = state.dateStringList[dayIndex];
        handleEmptyString(dateString, 'dateString');

        const { areaDict } = state.inflatedData;
        
        if(null == areaDict[areaId]){
            // Let the user know that there isn't an area with that areaId.
            // Early return
            return 
        }

        mutate('currentAreaData', areaDict[areaId]);
        mutate('areaDateString',  dateString);
    }
    // UPDATE
    ,async add_wsTask(taskName){
        const areaId = state.currentAreaId;
        if(null == areaId){
            // Early return
            return
        }
        
        handleEmptyString(taskName, 'taskName');

        const dayIndex  = state.dayIndex  || 0;
        const hourIndex = state.hourIndex || 0;

        ws_addTask(proxy__wsDateList, dayIndex, areaId, hourIndex, taskName);
        await setCoreState(clone(proxy__wsDateList), true);
    }
    ,async update_wsTask(taskIndex, duration){
        const areaId = state.currentAreaId;
        if(null == areaId){
            // Early return
            return
        }

        handleNumber(taskIndex, 'taskIndex');
        handleNumber(duration,  'duration');
        const dayIndex   = state.dayIndex  || 0;
        const hourIndex  = state.hourIndex || 0;

        ws_updateProxyTask(proxy__wsDateList, dayIndex, areaId, hourIndex, taskIndex, duration);
        await setCoreState(clone(proxy__wsDateList), true);
        await ws_put_task(proxy__wsDateList[dayIndex], FAKE_STORE_ID);

        commandDict[dayIndex] = clone(proxy__wsDateList[dayIndex]);
    }
    ,async move_wsTask(toHourIndex, taskName){
        handleNumber(toHourIndex,   'toHourIndex');
        handleEmptyString(taskName, 'taskName');

        const areaId = state.currentAreaId;
        if(null == areaId){
            // Early return
            return
        }
        
        const dayIndex      = state.dayIndex  || 0;
        const fromHourIndex = state.hourIndex || 0;
        // API CALL
        await ws_moveTask(proxy__wsDateList, dayIndex, areaId, fromHourIndex, toHourIndex, taskName);

        await setCoreState(clone(proxy__wsDateList), true);
        commandDict[dayIndex] = clone(proxy__wsDateList[dayIndex]);

        mutate('hourIndex', toHourIndex);
    }
    ,async update_wsShift(shiftInput){
        if(null == proxy__wsDateList){
            // Early return
            return
        }
        handleNumber(shiftInput, 'shiftInput');

        const dayIndex  = state.dayIndex  || 0;
        const hourIndex = state.hourIndex || 0;

        const {allAreasSchedule} = proxy__wsDateList[dayIndex];

        allAreasSchedule[hourIndex].shift = shiftInput;

        await setCoreState(proxy__wsDateList, true);
        
        ws_put_shift(proxy__wsDateList[dayIndex], FAKE_STORE_ID);
        commandDict[dayIndex] = clone(proxy__wsDateList[dayIndex]);
    }
    // DELETE
    ,async remove_wsTask(taskName){
        handleEmptyString(taskName, 'taskName');

        const areaId = state.currentAreaId;
        if(null == areaId){
            // Early return
            return
        }
        handleEmptyString(areaId,   'areaId');

        const dayIndex   = state.dayIndex  || 0;
        const hourIndex  = state.hourIndex || 0;
        handleNumber(dayIndex,  'dayIndex');
        handleNumber(hourIndex, 'hourIndex');

        ws_removeTask(proxy__wsDateList, dayIndex, areaId, hourIndex, taskName);
        await setCoreState(clone(proxy__wsDateList), true);

        commandDict[dayIndex] = clone(proxy__wsDateList[dayIndex]);
    }
    // SAVE
    ,async save(){
        state.canSave    = false;
        state.isUpdating = true;

        for(let dayIndex in commandDict){
            const response = await updateWS(commandDict[dayIndex]);
            console.log(response);
            console.log(commandDict[dayIndex])
            delete commandDict[dayIndex];
        }
        state.isUpdating = false;
    }
    // SETTINGS
    ,async read_itemsPerPage(){
        const itemsPerPage = await read_itemsPerPage();
        handleNumber(itemsPerPage, 'itemsPerPage');
        mutate('itemsPerPage', itemsPerPage);
    }
    ,async update_itemsPerPage(itemsPerPage){
        handleNumber(itemsPerPage, 'itemsPerPage');
        await update_itemsPerPage(itemsPerPage);
       actions.read_itemsPerPage();
    }
    ,async playTrack(){
        memento.play();
    }
}

/* GETTERS */
const getters = reactive({
    areaTimezoneDict: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }
        const {areaTimezoneDict} = state.inflatedData;
        if(null == areaTimezoneDict){
            // Early return
            return
        }
        const dayIndex      = getters.dayIndex  || 0;
        const hourIndex     = getters.hourIndex || 0;
        const currentAreaId = getters.currentAreaId;

        if(null == currentAreaId){
            // Early return
            return
        }
        return areaTimezoneDict
    })
    ,areaList: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }

        const {areaList} = state.inflatedData;
        if(null == areaList){
            // Early return
            return
        }
        return areaList;
    })
    ,areaReportList: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }
        const {allTimezoneDict} = state.inflatedData;
        const areaDateString    = state.areaDateString;
        const hourIndex         = state.hourIndex;
        if(null == allTimezoneDict || null == hourIndex || null == areaDateString){
            // Early return
            return
        }

        const dayData =  allTimezoneDict[areaDateString];
        if(null == dayData){
            // Early return
            return
        }

        const areaReportList = dayData[hourIndex];
        if(null == areaReportList){
            // Early return
            return
        }

        return areaReportList
    })
    ,areaTaskList: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }
        const {allTimezoneDict} = state.inflatedData;

        const areaDateString  = state.areaDateString;
        const hourIndex       = state.hourIndex;
        const currentAreaId   = state.currentAreaId;
        if(null == allTimezoneDict || null == hourIndex || null == areaDateString || null == currentAreaId){
            // Early return
            return
        }

        const dayData =  allTimezoneDict[areaDateString];
        if(null == dayData){
            // Early return
            return
        }

        const areaReportList = dayData[hourIndex];
        if(null == areaReportList){
            // Early return
            return
        }

        const areaData = areaReportList.find(({areaId})=>{
            return areaId == currentAreaId
        });

        if(null == areaData){
            // Early return
            return
        }
        handleDataStructure(areaData, 'areaData');
        const {areaTaskList} = areaData;
        return areaTaskList
    })
    ,areaName: computed(()=>{
        const areaId       = state.currentAreaId;
        if(null == areaId || null == state.inflatedData){
            // Early return
            return
        }

        const { areaDict } = state.inflatedData;
        handleEmptyString(areaId, 'areaId');

        if(null == areaDict[areaId]){
            // Let the user know that there isn't an area with that areaId.
            // Early return
            return 
        }
        const currentAreaData = areaDict[areaId];
        if(null == currentAreaData){
            // Early return
            return
        }
        
        handleDataStructure(currentAreaData, 'currentAreaData');
        const {area} = currentAreaData;
        return area.areaName
    })
    ,averageDict: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }
        const {averageDict} = state.inflatedData;
        if(null == averageDict){
            // Early return
            return
        }
        return averageDict;
    })

    ,canSave: computed(()=>{
        const canSave    = state.canSave;
        const isUpdating = state.isUpdating;
        
        console.log('getters canSave', canSave);
        console.log('getters isUpdating', isUpdating);

        return canSave && !isUpdating
    })
    ,currentAreaId: computed(()=>{
        const areaId = state.currentAreaId;
        if(null == areaId){
            // Early return
        }
        return state.currentAreaId;
    })
    ,currentAreaName: computed(()=>{
        const areaId = state.currentAreaId;
        if(null == areaId || null == state.inflatedData){
            // Early return
            return
        }
        handleEmptyString(areaId, 'areaId');
        const { areaDict } = state.inflatedData;
        if(null == areaDict){
            // Early return
        }
        handleDataStructure(areaDict, 'areaDict');
        const {area} = areaDict[areaId];

        if(null == area){
            // Let the user know that there isn't an area with that areaId.
            // Early return
            return 
        }
        const {areaName}= area;
        handleEmptyString(areaName, 'areaName');
        
        return areaName;
    })
    ,currentAreaData: computed(()=>{
        const areaId       = state.currentAreaId;
        if(null == areaId || null == state.inflatedData){
            // Early return
            return
        }

        const { areaDict } = state.inflatedData;
        handleEmptyString(areaId, 'areaId');

        if(null == areaDict[areaId]){
            // Let the user know that there isn't an area with that areaId.
            // Early return
            return 
        }
        const currentAreaData = areaDict[areaId];
        if(null == currentAreaData){
            // Early return
            return
        }
        
        handleDataStructure(currentAreaData, 'currentAreaData');
        return currentAreaData
    })

    ,dateWithDataList: computed(()=>{
        const dateWithDataList = state.dateWithDataList;
        if(null == dateWithDataList){
            // Early return
            return
        }
        return dateWithDataList
    })    
    ,dateList: computed(()=>{
        const dateList = state.dateList;
        if(null == dateList){
            // Early return
            return
        }
        return dateList
    })
    ,dayIndex: computed(()=>{
        const dayIndex = state.dayIndex || 0;
        handleNumber(dayIndex, 'dayIndex');
        return dayIndex
    })
    
    ,endDateString: computed(()=>{
        const endDateSet = state.endDateSet;
        if(null == endDateSet){
            // Early return
            return endDateSet
        }
        return endDateSet.dateString
    })
    
    ,hourList: computed(()=>{
        const hourList = state.hourList;
        handleArray(hourList, 'hourList');
        return hourList
    })
    ,hourIndex: computed(()=>{
        const hourIndex = state.hourIndex || 0;
        handleNumber(hourIndex, 'hourIndex');
        return hourIndex
    })
    ,hourString: computed(()=>{
        return state.hourList[state.hourIndex || 0]
    })
    ,hourIndexHover: computed(()=>{
        const hourIndexHover = state.hourIndexHover;
        handleIfNotNull(handleNumber, hourIndexHover, 'hourIndexHover');
        return hourIndexHover
    })
    
    ,itemsPerPage:computed(()=>{
        const itemsPerPage = state.itemsPerPage;
        const totalAreas   = state.totalAreas;
        if(null == itemsPerPage && null == totalAreas){
            // Early return
            return
        }

        if(null == itemsPerPage){
            return state.totalAreas;
        }
        
        handleNumber(itemsPerPage, 'itemsPerPage');
        return itemsPerPage
    })
    ,issueIndex: computed(()=>{
        const issueIndex = state.issueIndex;
        if(null == issueIndex){
            return 0
        }
        handleNumber(issueIndex, 'issueIndex');
        return issueIndex
    })
    ,isAverage: computed(()=>{
        return state.isAverage
    })
    
    ,newPath:computed(()=>{
        const newPath = state.newPath;
        if(null == newPath){
            // Early return
            return
        }
        handleEmptyString(newPath);
        return newPath
    })
    
    ,startDateString: computed(()=>{
        const startDateSet = state.startDateSet;
        if(null == startDateSet){
            // Early return
            return
        }
        return startDateSet.dateString
    })
    ,shiftDataList: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }
        const {shiftDataList} = state.inflatedData;
        if(null == shiftDataList){
            // Early return
            return
        }
        return shiftDataList
    })
    
    ,timezoneTaskList: computed(()=>{
        if(null == state.inflatedData){
            // Early return
            return
        }
        const {allTimezoneDict} = state.inflatedData;

        const areaDateString  = state.areaDateString;
        const hourIndex       = state.hourIndex;
        const currentAreaId   = state.currentAreaId;
        if(null == allTimezoneDict || null == hourIndex || null == areaDateString || null == currentAreaId){
            // Early return
            return
        }

        const dayData =  allTimezoneDict[areaDateString];
        if(null == dayData){
            // Early return
            return
        }

        const areaReportList = dayData[hourIndex];
        if(null == areaReportList){
            // Early return
            return
        }

        const areaData = areaReportList.find(({areaId})=>{
            return areaId == currentAreaId
        });

        if(null == areaData){
            // Early return
            return
        }
        handleDataStructure(areaData, 'areaData');
        const {taskList} = areaData;
        return taskList
    })
    ,totalAreas:computed(()=>{
        const totalAreas = state.totalAreas;
        if(null == totalAreas){
            // Early return
            return 0
        }
        handleNumber(totalAreas, 'totalAreas');
        return totalAreas
    })
})

/* INITIALIZATIONS */
const dateWithDataList = [];
for(let date=5; date < 19; date++){
    const time = new Date(`February ${date}, 2024 00:00:00`).getTime();
    dateWithDataList.push(time);
}
mutate('dateWithDataList', dateWithDataList);

const hourList = [];
for(let hour=0; hour < 24; hour++){
    let hourString = hour + ':00';
    hourString = hour < 10 ? '0' + hourString : hourString;
    hourList.push(hourString);
}
mutate('hourList', hourList);

export {
    actions,
    getters,
    mutations
}


import { getDatesetFromString, getDatesetFromTime  } from '../../../../libs/DateUtils.js'

function getDateSetByString(inputDate){
    handleEmptyString(inputDate, 'inputDate');
    const dateSet = getDatesetFromString(inputDate);
    return dateSet
}

function getDateSetByTime(inputTime){
    handleNumber(inputTime, 'inputTime');
    const dateSet = getDatesetFromTime(inputTime);
    return dateSet
}

export { getDateSetByString, getDateSetByTime }
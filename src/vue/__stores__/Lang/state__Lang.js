/* VUE */
import { reactive, computed, readonly } from 'vue'

/* DATA PROVIDERS */
import { get_man_hour_langs } from './dataProvider__Lang/dataProvider__get_man_hour_lang.js'

const DEFAULT_LANG = 'JP';

/* METHOD */

/* STATE */
const manHour = get_man_hour_langs();

const state = reactive({
    lang: DEFAULT_LANG,
    manHourLang: manHour[DEFAULT_LANG]
});

/* ACTIONS */
const actions ={
    async set_lang(lang){
        handleNotValidString(lang, ['EN', 'JP', 'TW'], 'lang');
        state.lang        = lang;
        state.manHourLang = manHour[lang];
    },
    localize(text){
        const localization = state.manHourLang[text] || text;
        return localization
    }
}

/* GETTERS */
const getters = reactive({
    manHourLang: computed(()=>{
        return readonly(state.manHourLang)
    }),
    lang: computed(()=>{
        return state.lang
    })
});

const localize = actions.localize;

export {
    actions,
    localize,
    getters
}
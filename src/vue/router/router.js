import { 
   createRouter
  ,createWebHistory
} from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // Initial
    {
      name: '',
      path: '/',
      component:  () => import('../pages/main/Main.vue'),
      props: true,
      redirect: {path:'/home/from/2024-02-05/to/2024-02-18/day/0/hour/0/issue/0'},
      children: [
        {
          name: 'home',
          path: `/home/from/:${'from'}/to/:${'to'}/day/:${'dayIndex'}/hour/:${'hourIndex'}/issue/:${'issueIndex'}`,
          component:  () => import('../pages/ws/home/1_PageHome.vue'),
          props: true
        },
        {
          name: 'area',
          //`/area/from/:${'from'}/to/:${'to'}/day/:${'dayIndex'}/hour/:${'hourIndex'}`,
          path: `/area/:${'areaId'}/from/:${'from'}/to/:${'to'}/day/:${'dayIndex'}/hour/:${'hourIndex'}/issue/:${'issueIndex'}/average/:${'isAverage'}`,
          component:  () => import('../pages/ws/details/2_PagePerformanceAnalysis.vue'),
          props: true
        }
      ]
    }
  ]
})

export default router
import { fileURLToPath, URL } from 'url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({ 
        registerType: 'autoUpdate',
        devOptions: {
          enabled: true
        },
        workbox: {
          clientsClaim: true,
          skipWaiting: true
        },
        manifest: {
          name: "tx",
          short_name: "tx",
          theme_color: "#333333",
          start_url: "/",
          display: "standalone",
          background_color: "#333333",
          icons: [
            {
              "src":"img/icons/pwa-192x192.png",
              "sizes":"192x192",
              "type":"image/png"
            },
            {
                "src":"img/icons/pwa-512x512.png",
                "sizes":"512x512",
                "type":"image/png"
            }
          ],
        },
      })
    ],
  base: '/',
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
